const fse = require('fs-extra');
const path = require('path');

const publicDir = path.resolve(__dirname, '../public');

fse.copySync(publicDir, path.join(__dirname, "/public"), { overwrite: true });