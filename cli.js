const fs = require("fs");
const path = require('path');

const CONSOLE_ARGUMENTS = ["-url"];

const COMPONENT_TEMPLATE = (name) => `import React from 'react';
import './style.scss';

export interface ${name}Props {
    
}

export default function ${name}({  }: ${name}Props) {
    return(
    
    );
}
`;

function createJsFile(folder, name) {

    fs.writeFile(path.join(folder, name + ".tsx"), COMPONENT_TEMPLATE(name), (err) => {
        if (err) {
            console.error(err);
        }
    })
}

function createCssFile(folder) {
    fs.writeFile(path.join(folder, "style.scss"), "", (err) => {
        if (err) {
            console.error(err);
        }
    })
}

function createFolder(folder) {
    try {
        if (!fs.existsSync(folder)){
            fs.mkdirSync(folder)
        }
    } catch (err) {
        console.error(err)
    }
}

function createComponent(creationRules) {
    creationRules.names.forEach(name => {
        const absolutePath = path.join(creationRules.url, name);
        createFolder(absolutePath);
        createJsFile(absolutePath, name);
        createCssFile(absolutePath);
     });
}

function getAbsolutePath(url) {
    let absolutePath = path.resolve(url);

    if(fs.existsSync(absolutePath)) return absolutePath;
    else return process.env.INIT_CWD;
}

function main(){

    let creationRules = {
        url: process.env.INIT_CWD,
        names: []
    };

    let arguments = process.argv;
    arguments.splice(0, 2);

    let isArgumentsValid = true;

    arguments.forEach((arg, index) => {
        switch (arg) {
            case CONSOLE_ARGUMENTS[0]: {
                creationRules.url = getAbsolutePath(arguments[index + 1]);
                break;
            }
            default: {
                if(arg[0] !== "-") {
                    creationRules.names.push(arg);
                } else {
                    console.log("Param " + arg + " is not exist. Correct params: -url");
                    isArgumentsValid = false;
                }
                break;
            }
        }
    });

    if(isArgumentsValid) {
        creationRules.names = creationRules.names.map(name => name.replace(",", ""));
        creationRules.names = creationRules.names.filter(name => name !== creationRules.url && /^[a-zA-Z]*$/g.test(name));

        createComponent(creationRules);
    }
}

main();

