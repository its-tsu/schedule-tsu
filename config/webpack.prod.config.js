const path = require('path');
const {merge} = require('webpack-merge');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const baseWebpackConfig = require('./webpack.base.config');

const prodWebpackConfig = merge(baseWebpackConfig, {

    mode: 'production',

    output: {
        path: path.join(__dirname, '../public'),
        filename: '[name].[contenthash].bundle.min.js',
        chunkFilename: '[name].[contenthash].chunks.min.js',
        publicPath: '/'
    },
    resolve: {
        modules: ['node_modules'],
        extensions: [".ts", ".tsx", ".js"]
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader'
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.(jpe?g|png|gif)$/,
                loader: 'url-loader',
                options: {
                    // Images larger than 10 KB won’t be inlined
                    limit: 10 * 1024,
                    name: 'assets/images/[name].[hash:8].[ext]',
                    esModule: false
                }
            },
            {
                test: /\.svg$/,
                loader: 'svg-url-loader',
                options: {
                    // Images larger than 10 KB won’t be inlined
                    limit: 10 * 1024,
                    name: 'assets/images/[name].[hash:8].[ext]',
                    // Remove quotes around the encoded URL –
                    // they’re rarely useful
                    noquotes: true,
                    esModule: false
                }
            },
            {
                test: /\.(jpg|png|gif|svg)$/,
                use: [
                    {
                        loader: 'image-webpack-loader',
                        // Specify enforce: 'pre' to apply the loader
                        // before url-loader/svg-url-loader
                        // and not duplicate it in rules with them
                        options: {
                            options: 'pre',
                            esModule: false
                        }
                    }
                ]
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            esModule: false,
                            name: 'assets/fonts/[name].[hash:8].[ext]',
                        },
                    }
                ],
            },
        ]
    },
    optimization: {
        splitChunks: {
            chunks: 'all'
        }
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: './src/index.html',
            inject: 'body'
        }),
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[name]-[id].css'
        }),
        new CopyPlugin({
            patterns: [
                {from: './favicon.jpg', to: './favicons', flatten: true},
            ],
        }),

    ]
});

module.exports = new Promise((resolve) => {
    resolve(prodWebpackConfig);
});