const { merge } = require('webpack-merge');
const path = require('path');
const baseWebpackConfig = require('./webpack.base.config');

const devWebpackConfig = merge(baseWebpackConfig, {

    mode: 'development',

    output: {
        path: path.join(__dirname, '../public'),
        filename: 'bundle.js',
        chunkFilename: '[name].[contenthash].chunks.min.js',
        publicPath: '/'
    },
    devtool: 'source-map',
    devServer: {
        hot: true,
        historyApiFallback: true,
        overlay: {
            errors: true
        }
    },
    resolve: {
        modules: ['node_modules'],
        extensions: [".ts", ".tsx", ".js"],
        alias: {
            'react-dom': '@hot-loader/react-dom'
        }
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.scss$/,
                use: ['style-loader', 'css-loader', 'sass-loader']
            },
            {
                test: /\.(jpe?g|png|gif)$/,
                loader: 'url-loader',
                options: {
                    esModule: false
                }
            },
            {
                test: /\.svg$/,
                loader: 'svg-url-loader'
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            esModule: false,
                            name: 'assets/fonts/[name].[ext]',
                        }
                    }
                ]
            },
        ]
    }
});

module.exports = new Promise((resolve) => {
    resolve(devWebpackConfig);
});