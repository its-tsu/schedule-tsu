declare module '*.png';
declare module '*.jpg';
declare module '*.json';
declare module '*.svg';
declare module 'react-adaptive';
declare module 'classnames';
declare module 'react-transition-group';
declare module 'nanoid';