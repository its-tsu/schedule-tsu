import React from 'react';
import './style.scss';
import classNames from 'classnames';

export interface DropdownButtonArrowProps {
    isOpen: boolean;
    className?: string
}

export default function DropdownButtonArrow({ isOpen, className = "" }: DropdownButtonArrowProps) {
    return(
        <div className={classNames(
            "dropdown-button__icon", className,
            { "_up": isOpen }
        )}>
            <svg width="10" height="5" viewBox="0 0 10 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 0L5 5L10 0H0Z"/>
            </svg>
        </div>
    );
}
