import React from 'react';
import './style.scss';
import classNames from 'classnames';

export interface NotificationCardProps {
    message: string;
    onClose: () => void;
}

export default function NotificationCard({ message, onClose }: NotificationCardProps) {
    return(
        <div className={classNames(
            "notification-card",
            { "_visible": message }
        )}>
            <div className="notification-card__content-wrapper">
                <p>{message}</p>
                <button type="button" className="notification-card__close-button" onClick={onClose}/>
            </div>
        </div>
    );
}
