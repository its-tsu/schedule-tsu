import React from 'react';
import './style.scss';

export interface SwitchButtonProps {
    value: string;
    onClick: () => void;
}

export default function SwitchButton({ value, onClick }: SwitchButtonProps) {
    return(
        <button type="button" className="switch-button" onClick={onClick}>{value}</button>
    );
}
