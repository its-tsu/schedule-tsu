import React, { ReactNode } from 'react';
import './style.scss';
import Cookies from "cookies-ts";
import { Redirect } from 'react-router-dom';
import { PATHS } from "../../../utils/constants";
import AdminLabel from "../AdminLabel/AdminLabel";

const cookies = new Cookies();

export interface AdminWrapperProps {
    children: ReactNode
}

export default function AdminWrapper({ children }: AdminWrapperProps) {

    if (!cookies.get("token")) {
        return <Redirect to={PATHS.adminRoot + PATHS.adminLogin}/>;
    }

    return(
        <div className="admin-wrapper">
            <AdminLabel/>
            { children }
        </div>
    );
}
