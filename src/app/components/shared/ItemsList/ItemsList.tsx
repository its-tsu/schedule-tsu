import React, {useCallback, useContext, useEffect, useState} from 'react';
import './style.scss';
import {BaseModel} from "../../../models/BaseModel";
import {AppContext} from "../../../App";
import {useLocation, NavLink} from "react-router-dom";
import moment from "moment";
import {ApiService} from "../../../services/ApiService";
import {PATHS} from "../../../utils/constants";
import {ARRAYS, BROWSER} from "../../../utils/functions";

const apiService = new ApiService();

export interface ItemsListProps {
    searchParam: string | number;
    rootItemId: number;
    type: string;
}

export default function ItemsList({ searchParam, rootItemId, type }: ItemsListProps) {
    const currentDate = moment();
    const searchParamDate = currentDate.day() !== 0 ? currentDate.toISOString() : currentDate.add(1, 'days').toISOString();

    const location = useLocation();
    const { addLoader, removeLoader, addErrorMessage } = useContext(AppContext);
    const addLoaderCallback = useCallback(addLoader, []);
    const removeLoaderCallback = useCallback(removeLoader, []);
    const addErrorMessageCallback = useCallback(addErrorMessage, []);
    const [rootPath, setRootPath] = useState("");
    const [itemsList, setItemsList] = useState<Array<BaseModel>>([]);

    useEffect(() => {
        addLoaderCallback("DROPDOWN_ITEMS_LOADER-" + searchParam);

        apiService.getListItems(type, rootItemId, searchParam)
            .then(res => {
                setItemsList(res);
            })
            .catch(() => addErrorMessageCallback("Ошибка! Не удалось загрузить элементы списка."))
            .finally(() => removeLoaderCallback("DROPDOWN_ITEMS_LOADER-" + searchParam));
    }, [rootItemId, type, searchParam, addLoaderCallback, removeLoaderCallback, addErrorMessageCallback]);

    useEffect(() => {
        setRootPath(location.pathname.includes(PATHS.terminalsRoot) ? PATHS.terminalsRoot : "");
    }, [location.pathname]);

    return(
        <>
            {
                !!itemsList.length &&
                itemsList.sort((a, b) => ARRAYS.sortByString(a.name, b.name))
                    .map(item => (
                        <NavLink key={item.id}
                                 to={`${rootPath}/${type}/${item.id}?isWeek=${!BROWSER.isMobile()}&selectedDate=${searchParamDate}`}
                                 className={`dropdown-list__window__item _${type}`}
                        >{ item.name }</NavLink>
                    ))
            }
        </>
    );
}
