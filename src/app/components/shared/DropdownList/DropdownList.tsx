import React, {ReactElement, useState} from 'react';
import './style.scss';
import './adaptive.scss';
import classNames from 'classnames';
import DropdownButtonArrow from '../DropdownButtonArrow/DropdownButtonArrow';

export interface DropdownListProps {
    title: string;
    children: ReactElement | ReactElement[];
}

export default function DropdownList({ title, children }: DropdownListProps) {
    const [isOpen, onOpenStateChange] = useState(false);
    
    return (
        <div className="dropdown-list container-laptop">
            <button className={classNames(
                "dropdown-list__open-button",
                { "_active": isOpen }
            )} type="button" onClick={() => onOpenStateChange(!isOpen)}>
                <h3 className="dropdown-list__open-button__title">{title}</h3>
                <DropdownButtonArrow isOpen={isOpen}/>
            </button>

            <div className={classNames(
                "dropdown-list__window",
                { "_open": isOpen }
            )}>
                { children }
            </div>
        </div>
    );
}
