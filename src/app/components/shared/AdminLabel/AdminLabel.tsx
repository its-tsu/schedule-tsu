import React from 'react';
import './style.scss';
import * as LogoTSU from "../../../media/images/tsu-main-logo.png";

export default function AdminLabel() {
    return(
        <div className="admin-label">
            <img src={LogoTSU} alt="Логотип ТГУ" width="320"/>
            <h1 className="admin-label__title">Панель управления</h1>
        </div>
    );
}
