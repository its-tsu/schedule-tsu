import React, { useCallback, useContext, useEffect, useState } from 'react';
import './style.scss';
import { FormattedItem } from '../../../classes/search.classes';
import { getScheduleListBySearchParam } from '../../../api/search.api';
import classNames from 'classnames';
import { SearchResponse } from "../../../models/response/SearchResponse";
import { NavLink, useHistory, useLocation } from "react-router-dom";
import { PATHS } from "../../../utils/constants";
import { AppContext } from "../../../App";
import { BROWSER, INPUT_LABEL_ID } from "../../../utils/functions";
import axios, { CancelTokenSource } from "axios";
import moment from "moment";

export interface SearchComponentProps {
    className: string;
}

const CancelToken = axios.CancelToken;
let source: CancelTokenSource | null;

export default function SearchComponent({ className = '' }: SearchComponentProps) {

    const currentDate = moment();
    const searchParamDate = currentDate.day() !== 0 ? currentDate.toISOString() : currentDate.add(1, 'days').toISOString();

    const history = useHistory();
    const location = useLocation();
    const { addErrorMessage } = useContext(AppContext);

    const [inputId] = useState(INPUT_LABEL_ID.newId());

    const inputRef = React.useRef<HTMLInputElement>(null);

    const addErrorMessageCallback = useCallback(addErrorMessage, []);

    const [isInputOpen, changeInputState] = useState(false);
    const [searchItemsList, fillSearchItems] = useState<Array<SearchResponse>>([]);
    const [currentItemIndex, changeCurrent] = useState(0);
    const [isWindowOpen, changeWindowState] = useState(false);
    const [rootPath, setRootPath] = useState("");

    function onInputChange(event: React.ChangeEvent<HTMLInputElement>): void {
        if (source) {
            source.cancel();
        }

        source = CancelToken.source();

        getScheduleListBySearchParam(event.target.value, source)
            .then(res => fillSearchItems(res.reverse()))
            .catch(error => {
                if (error !== "canceled") {
                    addErrorMessageCallback("Ошибка! Не удалось выполнить запрос.");
                }
            });
    }

    useEffect(() => {
        setRootPath(location.pathname.includes(PATHS.terminalsRoot) ? PATHS.terminalsRoot : "");
    }, [location.pathname]);

    function onKeyPressed(event: React.KeyboardEvent<HTMLInputElement>): void {

        if (
            !isWindowOpen
            || !searchItemsList.length
            || currentItemIndex === -1
            || !inputRef.current
            || !inputRef.current.value
        ) return;

        switch (event.key) {
            case "ArrowUp": {
                event.preventDefault();
                changeCurrent(currentItemIndex === 0 ? searchItemsList.length - 1 : currentItemIndex - 1);
                break;
            }
            case "ArrowDown": {
                event.preventDefault();
                changeCurrent(currentItemIndex === searchItemsList.length - 1 ? 0 : currentItemIndex + 1);
                break;
            }
            case "Enter": {
                event.preventDefault();
                inputRef.current.blur();
                history.push(`/${searchItemsList[currentItemIndex].type}/${searchItemsList[currentItemIndex].id}?isWeek=${!BROWSER.isMobile()}&selectedDate=${searchParamDate}`);
                break;
            }
            default:
                break;
        }
    }

    return (
        <section className={"search-component " + className}>
            <label className="search-component__field-block" htmlFor={inputId}>
                <button
                    type="button"
                    className="search-component__field-block__search-button"
                    onClick={() => changeInputState(true)}
                    disabled={isInputOpen}
                />
                <div className={classNames(
                    "input-block",
                    { "_open": isInputOpen }
                )}>
                    <label>
                        <input type="text"
                               id={inputId}
                               placeholder="Найти расписание"
                               onChange={onInputChange}
                               onKeyDown={onKeyPressed}
                               onFocus={() => changeWindowState(true)}
                               onBlur={() => setTimeout(() => changeWindowState(false), 300)}
                               ref={inputRef}
                        />
                    </label>
                    <button className="hide-field-button" type="button" onClick={() => changeInputState(false)}/>
                </div>
            </label>
            {
                isWindowOpen
                && !!searchItemsList.length
                && inputRef.current
                && inputRef.current.value
                && <nav className="search-component__list"
                        onMouseEnter={() => changeCurrent(-1)}
                        onMouseLeave={() => changeCurrent(0)}
                >
                    {
                        searchItemsList
                            .map((item: FormattedItem, index: number) => (
                            <NavLink key={index}
                               to={`${rootPath}/${item.type}/${item.id}?isWeek=${!BROWSER.isMobile()}&selectedDate=${searchParamDate}`}
                               className={classNames(
                                   "search-component__list__search-item",
                                   { "_current": currentItemIndex === index }
                               )}>{item.name}</NavLink>
                        ))
                    }
                </nav>
            }
        </section>
    );
}
