import React from 'react';
import './style.scss';

export default function RootLoader() {
    return(
        <div className="root-loader">
            <div className="cube-loader__container">
                <div className="caption">
                    <div className="cube-loader">
                        <div className="cube loader-1"/>
                        <div className="cube loader-2"/>
                        <div className="cube loader-4"/>
                        <div className="cube loader-3"/>
                    </div>
                </div>
            </div>
        </div>
    );
}
