import React, { useRef, useState } from 'react';
import './style.scss';
import classNames from 'classnames';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import { useClickOutside } from 'use-events';
import DropdownButtonArrow from '../DropdownButtonArrow/DropdownButtonArrow';
import { BaseModel } from "../../../models/BaseModel";
import { ARRAYS } from "../../../utils/functions";

export interface DropdownButtonProps {
    currentItem: BaseModel;
    itemsList: Array<BaseModel>;
    className?: string;
    onSelect: (item: BaseModel) => void
}

export default function DropdownButton({ currentItem, itemsList, className= "", onSelect }: DropdownButtonProps) {

    const dropdownListRef = useRef(null);

    const [isOpen, setOpenState] = useState(false);

    useClickOutside([dropdownListRef], () => setTimeout(() => setOpenState(false), 300));

    return(
        <div className="dropdown-button" ref={dropdownListRef}>
            <button type="button" className={classNames(
                "dropdown-button__button",
                className,
                {
                    "_active": isOpen,
                    "_disabled": !itemsList.length
                }
            )} onClick={() => setOpenState(!isOpen)}>
                <p className="dropdown-button__button__title">{currentItem.name.length > 35 ? currentItem.name.slice(0, 35) + "..." : currentItem.name}</p>
                <DropdownButtonArrow isOpen={isOpen} className="dropdown-button__button__arrow"/>
            </button>
            <TransitionGroup>
                {
                    isOpen && itemsList.length
                    && <CSSTransition timeout={300} classNames="dropdown-list-wrapper">
                        <ul className="dropdown-button__list">
                            {
                                itemsList
                                    .filter(item => item.name !== currentItem.name)
                                    .sort((a, b) => ARRAYS.sortByString(a.name, b.name))
                                    .map(item => (
                                    <li className="dropdown-button__list__item" onClick={() => {
                                        onSelect(item);
                                        setOpenState(!isOpen);
                                    }} key={item.name}>{item.name}</li>
                                ))
                            }
                        </ul>
                    </CSSTransition>
                }
            </TransitionGroup>
        </div>
    );
}
