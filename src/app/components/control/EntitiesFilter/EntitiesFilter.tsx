import React from 'react';
import './style.scss';
import DropdownButton from "../../shared/DropdownButton/DropdownButton";
import { BaseModel } from "../../../models/BaseModel";
import * as classNames from "classnames";

export interface EntitiesFilterProps {
    currentItem: BaseModel;
    itemsList: BaseModel[];

    isHidden: boolean;

    inputRef: (instance: HTMLInputElement | null) => void;

    onSearch: (value: string) => void;
    onItemSelect: (item: BaseModel) => void;

    onHiddenChange: (value: boolean) => void;
}

export default function EntitiesFilter({ currentItem, itemsList, isHidden, inputRef, onSearch, onItemSelect, onHiddenChange }: EntitiesFilterProps) {

    return(
        <div className="entities-filter">
            <div className="entities-filter__controls">
                <input className="entities-filter__field"
                       placeholder="Отфильтровать по имени"
                       ref={inputRef}
                       onChange={(event) => onSearch(event.target.value)}/>
                <DropdownButton currentItem={currentItem} itemsList={itemsList} onSelect={onItemSelect}/>
            </div>
            <div className="entities-filter__switch">
                <button type="button" className={classNames(
                    "entities-filter__switch-item",
                    { selected: !isHidden }
                )} onClick={() => onHiddenChange(false)}>Отображаемые</button>
                <button type="button" className={classNames(
                    "entities-filter__switch-item",
                    { selected: isHidden }
                )} onClick={() => onHiddenChange(true)}>Скрытые</button>
            </div>
        </div>
    );
}
