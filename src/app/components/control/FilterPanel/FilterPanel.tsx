import React, { useCallback, useContext, useEffect, useState } from 'react';
import './style.scss';
import { BaseModel  } from "../../../models/BaseModel";
import DropdownButton from "../../shared/DropdownButton/DropdownButton";
import { ApiService } from "../../../services/ApiService";
import { GroupResponse } from "../../../models/response/GroupResponse";
import { TeachersResponse } from "../../../models/response/TeachersResponse";
import { ClassroomResponse } from "../../../models/response/ClassroomResponse";
import SwitchButton from "../../shared/SwitchButton/SwitchButton";
import { AppContext } from "../../../App";

const apiService = new ApiService();

export interface FilterPanelProps {
    currentRootItem: BaseModel;
    currentCourse: BaseModel;
    currentItem: GroupResponse | TeachersResponse | ClassroomResponse | null;

    isWeek: boolean;
    type: string;

    onSettingChange: (setting: string, value: BaseModel) => void;
    onWeekStateChange: () => void;
    onTodayClick: () => void;
}

export default function FilterPanel({
                                        currentRootItem,
                                        currentCourse,
                                        currentItem,
                                        isWeek,
                                        type,
                                        onSettingChange,
                                        onWeekStateChange,
                                        onTodayClick
                                    }: FilterPanelProps) {

    const [rootViewList, setRootViewList] = useState<Array<BaseModel>>([]);
    const [currentGroupList, setCurrentGroupList] = useState<Array<BaseModel>>([]);
    const [currentItemsList, setCurrentItemsList] = useState<Array<BaseModel>>([]);

    const { addErrorMessage } = useContext(AppContext);
    const addErrorMessageCallback = useCallback(addErrorMessage, []);

    const onRootItemSelect = (item: BaseModel) => {
        onSettingChange("rootItem", item);
    };

    const onGroupItemSelect = (item: BaseModel) => {
        onSettingChange("groupItem", item);
    };

    const onItemSelect = (item: BaseModel) => {
        onSettingChange("item", item);
    };

    const getName = (item: GroupResponse | TeachersResponse | ClassroomResponse) => {
        if ("lastName" in item) {
            return `${item.lastName} ${item.name[0]}.${item.patronymic[0]}.`;
        }

        return item.name;
    };

    useEffect(() => {
        if (!currentRootItem.id) return;

        apiService.getRootItemsList(type)
            .then(res => setRootViewList(res))
            .catch(() => addErrorMessageCallback("Ошибка! Не удалось получить список по параметрам"));
        
        apiService.getItemsList(type, currentRootItem.id)
            .then(res => setCurrentGroupList(res))
            .catch(() => addErrorMessageCallback("Ошибка! Не удалось получить список по параметрам"));

        if ((type === "teacher" && !currentCourse.id) || (type !== "teacher" && !currentCourse.name)) return;

        apiService.getListItems(type, currentRootItem.id, currentCourse.id || currentCourse.name)
            .then(res => setCurrentItemsList(res.map((item) => new BaseModel(item.id, item.name))))
            .catch(() => addErrorMessageCallback("Ошибка! Не удалось получить список по параметрам"));
    }, [addErrorMessageCallback, currentCourse.id, currentCourse.name, currentRootItem.id, type]);

    return(
        <section className="filter-panel">
            <div className="filter-panel__first-group">
                <DropdownButton currentItem={currentRootItem} itemsList={rootViewList} onSelect={onRootItemSelect}/>
                <DropdownButton currentItem={currentCourse} itemsList={currentGroupList} onSelect={onGroupItemSelect}/>
                <DropdownButton currentItem={new BaseModel(currentItem ? currentItem.id : 0, currentItem ? getName(currentItem) : "")}
                                itemsList={currentItemsList} onSelect={onItemSelect}/>
            </div>
            <div className="filter-panel__second-group">
                <SwitchButton value="СЕГОДНЯ" onClick={onTodayClick}/>
                <SwitchButton value={isWeek ? "День" : "Неделя"} onClick={onWeekStateChange}/>
            </div>
        </section>
    );
}
