import React, { Ref } from 'react';
import './style.scss';
import moment, { Moment } from 'moment';
import 'moment/locale/ru';
import classNames from 'classnames';

import { Day, Week } from '../../../../classes/calendar.classes';

export interface CalendarWeekSlideProps {
    week: Week;
    currentDate: Moment;
    className?: string;

    isWeek: boolean;

    onItemClick: (item: Week | Day) => void,
}

function CalendarWeekSlide({ className= "", week, onItemClick, isWeek, currentDate }: CalendarWeekSlideProps, ref: Ref<HTMLLIElement>) {

    moment.locale('ru');

    function onClick(day: Day) {
        if(isWeek) {
            onItemClick(week);
        } else {
            onItemClick(day);
        }
    }

    return(
        <li className={classNames(
            "week-slide " + className,
            { "_active": isWeek && currentDate.format("W") === week.weekDays[0].date.format("W") }
        )} ref={ref}>
            {
                week.weekDays.map(day => (
                    <div className="button-block" key={day.id}>
                        {
                            day.dateNumber === 1 && <div className="first-day-line"/>
                        }
                        <button type="button" className={classNames(
                            "week-slide__day",
                            {
                                "_disabled": day.dayName === 'вс',
                                "_active": !isWeek && currentDate.format("DDD") === day.date.format("DDD"),
                            }
                        )} onClick={() => onClick(day)} key={day.id}>
                            {day.dayName}
                            <br/>
                            {day.dateNumber}
                        </button>
                    </div>
                ))
            }
        </li>
    );
}

export default React.forwardRef(CalendarWeekSlide);
