import React, { RefObject, useEffect, useRef, useState } from 'react';
import './style.scss';
import '../adaptive.scss';
import classNames from 'classnames';
import { Day, Week } from '../../../../classes/calendar.classes';
import CalendarWeekSlide from '../CalendarWeekSlide/CalendarWeekSlide';
import { CalendarService } from '../calendar.service';
import moment, { Moment } from 'moment';
import { ClientRect } from '../../../../classes/dom.classes';

const calendarService = new CalendarService();

function calculateShift(shift: number, currentElementCharacteristics: ClientRect) {
    return shift - currentElementCharacteristics.left
        + document.documentElement.getBoundingClientRect().right / 2
        - currentElementCharacteristics.width / 2;
}

function getElementCharacteristics(elementRef: RefObject<HTMLElement>) {
    if (elementRef && elementRef.current) {
        return elementRef.current.getBoundingClientRect();
    }

    return new ClientRect();
}

function getMonths(weeksList: Array<Week>, sliderRef: RefObject<HTMLElement>) {
    const monthsList: Array<Moment> = [];
    const sliderClientRect = getElementCharacteristics(sliderRef);

    weeksList.forEach(week => {
        const weekClientRect = getElementCharacteristics(week.elementRef);

        if(weekClientRect.left >= sliderClientRect.left && weekClientRect.left <= sliderClientRect.right) {
            monthsList.push(week.weekDays[0].date);
            monthsList.push(week.weekDays[6].date);
        }
    });

    if (monthsList.length) {
        return monthsList;
    } else {
        return [moment()];
    }
}

export interface CalendarSliderProps {
    currentDate: Moment;
    isWeek: boolean;

    onDateClick: (item: Week | Day) => void
}

export default function CalendarSlider({ isWeek = true, onDateClick, currentDate }: CalendarSliderProps) {

    const sliderWrapperRef = useRef(null);
    const sliderRef = useRef(null);

    const [weeksList, changeWeekList] = useState<Array<Week>>([...calendarService.getWeeksList()]);
    const [shift, changeShift] = useState(0);
    const [isPrevDisabled, changePrevDisable] = useState(false);
    const [isNextDisabled, changeNextDisable] = useState(false);
    const [months, setMonths] = useState<Array<Moment>>([currentDate]);
    const [isAnimationDisabled, setAnimationState] = useState(true);

    function onPrevClick() {
        const element = weeksList[0];

        changeShift((shift: number) => {
            const NEW_SHIFT = shift + getElementCharacteristics(element.elementRef).width;

            if (NEW_SHIFT > 0) {
                changePrevDisable(true);
                return 0;
            } else {
                changePrevDisable(false);
                return NEW_SHIFT;
            }
        });
    }

    function onNextClick() {
        const element = weeksList[weeksList.length - 1];
        changeShift((shift: number) => {
            const NEW_SHIFT = shift - getElementCharacteristics(element.elementRef).width;
            const sliderShift = -1 * getElementCharacteristics(sliderRef).width + getElementCharacteristics(sliderWrapperRef).width;

            if (NEW_SHIFT < sliderShift) {
                changeNextDisable(true);
                return sliderShift;
            } else {
                changePrevDisable(false);
                return NEW_SHIFT;
            }
        });
    }

    useEffect(() => {
        calendarService.initWeeksList().then(() => {
            changeWeekList(calendarService.getWeeksList());
            setTimeout(() => setAnimationState(false), 300);
        });
    }, []);

    useEffect(() => {
        const currentWeek = weeksList.find((week: Week) => currentDate.format("W") === week.weekDays[0].date.format("W"));

        if(currentWeek) {

            changeShift((shift: number) => {

                const NEW_SHIFT = calculateShift(shift, getElementCharacteristics(currentWeek.elementRef));

                if (NEW_SHIFT > 0) {
                    changePrevDisable(true);
                    return 0;
                } else {
                    changePrevDisable(false);
                    return NEW_SHIFT;
                }
            });
        }
    }, [currentDate, weeksList]);

    useEffect(() => {
        changeWeekList(calendarService.getWeeksList());
    }, [weeksList, shift]);

    return(
        <section className="calendar-slider">
            <button type="button"
                    className={classNames(
                        "calendar-slider__button button-prev",
                        { "_disabled": isPrevDisabled }
                    )}
                    onClick={onPrevClick}
            />
            <section className="calendar-slider__path" ref={sliderWrapperRef}>
                <div className="months-bar">
                    <h4>{`${months[0].format("MMMM")}, ${months[0].format("YYYY")}`}</h4>
                    {
                        months[1] && (months[0].format("M") !== months[1].format("M"))
                        && <h4>{`${months[1].format("MMMM")}, ${months[1].format("YYYY")}`}</h4>
                    }
                </div>
                <ul className={classNames(
                    "slides__list",
                    { "_animation-disabled": isAnimationDisabled }
                )}
                    ref={sliderRef}
                    style={{ transform: `translateX(${shift}px)` }} onTransitionEnd={() => setMonths(getMonths(weeksList, sliderWrapperRef))}>
                    {
                        weeksList.map((week: Week) => (
                            <CalendarWeekSlide week={week}
                                               isWeek={isWeek}
                                               onItemClick={onDateClick}
                                               key={week.id}
                                               ref={week.elementRef}
                                               currentDate={currentDate}
                            />
                        ))
                    }
                </ul>
            </section>
            <button type="button"
                    className={classNames(
                        "calendar-slider__button button-next",
                        { "_disabled": isNextDisabled }
                    )}
                    onClick={onNextClick}
            />
        </section>
    );
}
