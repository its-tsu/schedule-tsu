import { Day, Week } from '../../../classes/calendar.classes';
import moment, { Moment } from 'moment';
import 'moment/locale/ru';

export class CalendarService {
    currentDate: Moment = moment();
    weeksList: Array<Week> = [];

    constructor() {
        this.weeksList.push(this.createWeekByWeekDay(this.currentDate));
    }

    initWeeksList() {
        return new Promise((resolve) => {
            this.fillCalendar();

            resolve();
        });
    }

    getWeeksList() {
        return this.weeksList;
    }

    createWeekByWeekDay(weekDay: Moment) {
        const weekStart = weekDay.clone().startOf('isoWeek');
        const daysList: Array<Day> = [];

        for (let i = 0; i < 7; i++) {
            daysList.push(new Day(moment(weekStart).add(i, 'days')));
        }

        return new Week(daysList);
    }

    fillCalendar() {
        this.weeksList = [];

        const studyYearStart = CalendarService.getStudyYearStart();
        const maxDate = moment().day(60);
        const weeksCount = CalendarService.getWeeksCount(studyYearStart.clone(), maxDate.clone());

        for (let i = 0; i < weeksCount; i++) {
            this.weeksList.push(this.createWeekByWeekDay(studyYearStart.clone().isoWeek(studyYearStart.isoWeek() + i)));
        }
    }

    private static getStudyYearStart(): Moment {
        const currentDate = moment();

        if (currentDate.month() >= 8) {
            return moment([currentDate.year(), 8, 1]);
        } else {
            return moment([currentDate.year() - 1, 8, 1]);
        }
    }

    private static getWeeksCount(minDate: Moment, maxDate: Moment): number {
        const maxDateWeek = maxDate.isoWeek();
        const minDateWeek = minDate.isoWeek();

        if (maxDateWeek > minDateWeek) {
            return maxDateWeek - minDateWeek;
        }

        const lastWeek = moment().year(minDate.year()).month(11).date(30).isoWeek();

        return lastWeek - minDateWeek + maxDateWeek;
    }
}