import React, { useRef, MouseEvent, useContext } from 'react';
import './style.scss';
import { createUserRequest } from "../../../api/users.api";
import { AppContext } from "../../../App";

const CREATE_USER_LOADER_ID = "CREATE_USER_LOADER";

export interface CreateUserFormProps {
    onClose: () => void;
}

export default function CreateUserForm({ onClose }: CreateUserFormProps) {

    const { addLoader, removeLoader, addErrorMessage } = useContext(AppContext);

    const userData = {
        name: "",
        login: "",
        role: "",
        password: ""
    };

    const formRef = useRef<HTMLFormElement>(null);

    const createUser = () => {
        addLoader(CREATE_USER_LOADER_ID);
        
        createUserRequest(userData)
            .then(() => {
                onClose();
            })
            .catch(err => {
                addErrorMessage(err.date.message);
            })
            .finally(() => {
                removeLoader(CREATE_USER_LOADER_ID);
            });
    };

    const onClick = (event: MouseEvent<HTMLDivElement>) => {
        if (formRef && formRef.current && !formRef.current.contains(event.target as Node)) {
            onClose();
        }
    };

    return(
    <div className="form-wrapper" onClick={onClick}>
        <form className="create-user-form" ref={formRef}>
            <h3>Создание пользователя</h3>
            <label>
                <input className="create-user-form__field"
                       placeholder="Имя"
                       onChange={(event) => userData.name = event.target.value}/>
            </label>
            <label>
                <input className="create-user-form__field"
                       placeholder="Логин"
                       onChange={(event) => userData.login = event.target.value}/>
            </label>
            <label>
                <input className="create-user-form__field"
                       placeholder="Роль"
                       onChange={(event) => userData.role = event.target.value}/>
            </label>
            <label>
                <input className="create-user-form__field"
                       placeholder="Пароль"
                       onChange={(event) => userData.password = event.target.value}/>
            </label>
            <button type="button"
                    className="create-user-form__button"
                    onClick={createUser}>Создать</button>
        </form>
    </div>
    );
}
