import React, { useContext, useState } from 'react';
import './style.scss';

import { login as loginRequest } from "../../../api/admin.api";
import { AppContext } from "../../../App";
import Cookies from "cookies-ts";
import { useHistory } from 'react-router-dom';
import { PATHS } from "../../../utils/constants";

const cookies = new Cookies();

export default function AdminLoginForm() {

    const history = useHistory();

    const [login, setLogin] = useState("");
    const [password, setPassword] = useState("");

    const { addErrorMessage } = useContext(AppContext);

    const authenticate = async () => {
        try {
            const response = await loginRequest(login, password);

            console.log(response.data);
            cookies.set("token", response.data.token);
            cookies.set("role", response.data.role);
            history.push(PATHS.adminRoot);
        } catch (e) {
            addErrorMessage("Не удалось войти");
        }
    };

    return(
        <form className="admin-login-form" onSubmit={authenticate}>
            <h2 className="admin-login-form__title">Авторизация</h2>
            <div className="admin-login-form__field">
                <label htmlFor="adminLoginField" className="admin-login-form__field-label">Логин</label>
                <input id="adminLoginField" className="admin-login-form__field-input"
                       value={login} onChange={event => setLogin(event.target.value)}/>
            </div>
            <div className="admin-login-form__field">
                <label htmlFor="adminPasswordField" className="admin-login-form__field-label">Пароль</label>
                <input id="adminPasswordField" className="admin-login-form__field-input" type="password"
                       value={password} onChange={event => setPassword(event.target.value)}/>
            </div>
            <button type="button" className="admin-login-form__submit-button" onClick={authenticate}>Войти</button>
        </form>
    );
}
