import React from 'react';
import './style.scss';
import SearchComponent from '../../../shared/SearchComponent/SearchComponent';

export default function HeaderSearchField() {

    return (
        <SearchComponent className="header-search"/>
    );
}
