import React from 'react';
import './style.scss';
import classNames from "classnames";

export interface EmptyCell {
    isCurrent: boolean
}

export default function EmptyCell({ isCurrent }: EmptyCell) {
    return (
        <div className={classNames(
            "schedule-cell _empty",
            { "_current": isCurrent }
        )}>Нет занятий</div>
    );
}
