import React, { useEffect, useRef } from 'react';
import './style.scss';
import classNames from "classnames";
import { ScheduleResponse } from "../../../models/response/ScheduleResponse";
import moment from "moment";
import { typesColorsMap } from "../../../consts/types-colors";
import { BROWSER, DATES, STRINGS } from "../../../utils/functions";
import { NavLink } from "react-router-dom";

export interface ScheduleCellProps {
    scheduleList: Array<ScheduleResponse>,
    currentView: string,
    type: string,
}

export default function ScheduleCell({ scheduleList, currentView, type }: ScheduleCellProps) {

    const typeClassName = (schedule: ScheduleResponse) => typesColorsMap.get(schedule.type) ? typesColorsMap.get(schedule.type) : "_default";
    const currentGroupRef = useRef<HTMLParagraphElement>(null);

    useEffect(() => {
        if (currentGroupRef && currentGroupRef.current) {
            currentGroupRef.current.scrollIntoView();
        }
    }, []);

    return (
        <div className={classNames(
            "schedule-cell",
            { "_current": scheduleList[0].date === moment().format("YYYY.MM.DD") }
        )}>
            {
                scheduleList.map((schedule) => (
                    <div className="schedule-cell__schedule-group" key={schedule.id}>
                        <h4 className="schedule-cell__discipline-name">{schedule.disciplineName} ({schedule.type})</h4>
                        <div className="groups-list">
                            {
                                schedule.groupsList.map(group => (
                                    currentView === group.name
                                        ? <p className="groups-list__item _bold" key={group.id}
                                             ref={currentGroupRef}>{group.name}</p>
                                        : <NavLink
                                            to={"/group/" + group.id + "?isWeek=" + !BROWSER.isMobile() + "&selectedDate=" + moment(schedule.fromTime).toISOString()}
                                            key={group.id}>
                                            <p className="groups-list__item">{group.name}</p>
                                        </NavLink>

                                ))
                            }
                        </div>
                        <div className="schedule-cell__info">
                            <div className="teacher-classroom-block">
                                {
                                    !!schedule.teacher
                                        ? <NavLink
                                            to={"/teacher/" + schedule.teacher.id + "?isWeek=" + !BROWSER.isMobile() + "&selectedDate=" + moment(schedule.fromTime).toISOString()}>
                                            <p className="teacher-classroom-block__teacher">{STRINGS.getTeacherShortName(schedule.teacher)}</p>
                                        </NavLink>
                                        : <p/>
                                }
                                {
                                    !!schedule.classroom && <p className="teacher-classroom-block__classroom">{STRINGS.getClassroomName(schedule.classroom, schedule.note, type)}</p>
                                }
                            </div>
                        </div>
                        <div className="schedule-cell__time-and-link">
                            <p className="schedule-cell__time">[{DATES.getPairTimeByDates(schedule.fromTime, schedule.toTime)}]</p>
                        </div>

                        <svg className={classNames(
                            "type-icon",
                            typeClassName(schedule)
                        )} width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M14.5765 0H0.550293L14.5765 14.5V0Z" fill="#E0E0E0"/>
                        </svg>
                    </div>
                ))
            }
        </div>
    );
}
