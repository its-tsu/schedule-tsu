import React, { useEffect, useState } from 'react';
import './style.scss';
import './adaptive.scss';
import classNames from "classnames";
import { ScheduleResponse } from "../../../models/response/ScheduleResponse";
import moment, { Moment } from "moment";
import ScheduleCell from "../ScheduleCell/ScheduleCell";
import EmptyCell from "../EmptyCell/EmptyCell";

export interface ScheduleTableProps {
    scheduleList: Array<ScheduleResponse>;
    date: Moment;
    currentView: string,
    type: string,
}

export default function ScheduleTable({ scheduleList, date, currentView, type }: ScheduleTableProps) {

    const getConvertedList = (scheduleList: Array<ScheduleResponse>): {
        array: Array<Array<Array<ScheduleResponse>>>;
        min: number;
        max: number;
    } => {
        const pairNumbersSet = new Set<number>();
        const resultArray: Array<Array<Array<ScheduleResponse>>> = [[], [], [], [], [], [], [], []];

        for (let i = 0; i < 8; i++) {
            for (let j = 0; j < 8; j++) {
                resultArray[i][j] = [];
            }
        }

        scheduleList.forEach(schedule => {
            if (schedule.pairNumber <= 0) return;

            const dayOfWeek = moment(schedule.date, "YYYY.MM.DD").isoWeekday();
            resultArray[schedule.pairNumber - 1][dayOfWeek].push(schedule);
            pairNumbersSet.add(schedule.pairNumber);
        });

        const pairNumbersArray: Array<number> = Array.from(pairNumbersSet);

        return {
            min: Math.min(...pairNumbersArray),
            max: Math.max(...pairNumbersArray),
            array: resultArray
        };
    };

    const getDatesList = (date: Moment): Array<string> => {
        const monday = moment(date).clone().startOf("isoWeek");
        const datesList = [monday.format("dd D")];

        for (let i = 1; i < 6; i++) {
            const day = monday.clone().weekday(i);
            datesList.push(day.format("dd D"));
        }

        return datesList;
    };

    const [formattedScheduleList, setScheduleList] = useState<Array<Array<Array<ScheduleResponse>>>>([]);
    const [datesList, setDatesList] = useState<Array<string>>([]);
    const [maxPair, setMaxPair] = useState(1);
    const [isEmptyWeek, setEmptyState] = useState(false);

    useEffect(() => {
        if (scheduleList.length === 0 || scheduleList.every(schedule => schedule.pairNumber === 0)) {
            setEmptyState(true);
            return;
        }

        const convertedObject = getConvertedList(scheduleList);

        setScheduleList(convertedObject.array);
        setMaxPair(convertedObject.max);
        setDatesList(getDatesList(date));
        setEmptyState(false);
    }, [scheduleList, date]);

    return (
        <div className={classNames(
            "schedule-table__wrapper",
            { "_empty": isEmptyWeek }
        )}>
            <section className="schedule-table">
                {
                    !isEmptyWeek
                        ? <section className="schedule-table__cells">
                            <div className="empty-cell"/>
                            {
                                datesList.map((dateString, index) => <div className={classNames(
                                    "date-cell",
                                    { "_current": date.isoWeek() === moment().isoWeek() && (index + 1) === moment().isoWeekday() }
                                )} key={dateString}>{dateString.toUpperCase()}</div>)
                            }
                            <div className="empty-cell"/>
                            {
                                formattedScheduleList
                                    .filter((scheduleList, rowIndex) => rowIndex < maxPair)
                                    .map((scheduleList, rowIndex) => (
                                        scheduleList.map((schedule, colIndex) => (
                                            colIndex === 0 || colIndex % 7 === 0
                                                ? <div className="pair-number" key={(colIndex + 1) * (rowIndex + 1)}>{rowIndex + 1}</div>
                                                : schedule.length
                                                ? <ScheduleCell type={type} scheduleList={schedule} currentView={currentView} key={schedule[0].id}/>
                                                : <EmptyCell key={(colIndex + 1) * -1}
                                                             isCurrent={date.isoWeek() === moment().isoWeek() && colIndex === moment().isoWeekday()}/>
                                        ))
                                    ))
                            }
                        </section>

                        : <div className="empty-table">Нет занятий</div>
                }
            </section>
        </div>
    );
}
