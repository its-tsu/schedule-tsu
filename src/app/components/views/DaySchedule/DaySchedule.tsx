import React, { useEffect, useState } from 'react';
import './style.scss';
import './adaptive.scss';
import { ScheduleResponse } from "../../../models/response/ScheduleResponse";
import { Moment } from "moment";
import DayClass from "../DayClass/DayClass";
import DayClassEmpty from "../DayClassEmpty/DayClassEmpty";

export interface DayScheduleProps {
    scheduleList: Array<ScheduleResponse>,
    date: Moment,
    currentView: string,
    type: string,
}

export default function DaySchedule({ scheduleList, date, currentView, type }: DayScheduleProps) {

    const [formattedScheduleList, setScheduleList] = useState<Array<Array<ScheduleResponse>>>([]);
    const [maxNumber, setMaxNumber] = useState(1);

    useEffect(() => {
        const pairNumbersSet = new Set<number>();
        const formattedList: Array<Array<ScheduleResponse>> = [[], [], [], [], [], [], [], []];

        scheduleList.forEach(schedule => {
            if (schedule.pairNumber <= 0) return;

            formattedList[schedule.pairNumber - 1].push(schedule);
            pairNumbersSet.add(schedule.pairNumber);
        });

        const pairNumbersArray: Array<number> = Array.from(pairNumbersSet);

        setScheduleList(formattedList);

        if (pairNumbersArray.length) {
            setMaxNumber(Math.max(...pairNumbersArray));
        }
    }, [scheduleList]);

    return (
        <section className="day-schedule">
            <p className="day-schedule__date">{date.format("dddd, D")}</p>
            <section className="day-schedule__list">
                {
                    formattedScheduleList
                        .filter((schedule, index) => index + 1 <= maxNumber)
                        .map((schedule, index) => schedule.length
                        ? <DayClass type={type} scheduleList={schedule} currentView={currentView} key={schedule[0].id}/>
                        : <DayClassEmpty pairNumber={index + 1} key={index}/>
                    )
                }
            </section>
        </section>
    );
}
