import React, { useState } from 'react';
import classNames from 'classnames';
import { useClickOutside } from 'use-events';
import './style.scss';

export default function Services() {
    
    const servicesRef = React.useRef(null);

    const [isMenuOpen, changeMenuState] = useState(false);

    useClickOutside([servicesRef], () => changeMenuState(false));

    return(
        <div className="services" ref={servicesRef}>
            <button className="dropdown_button" type="button"
                    onClick={() => changeMenuState(!isMenuOpen)}>
                <div className="img"/>
                <span>Сервисы</span>
            </button>
            {/*Новые сервисы*/}
            <div className={classNames(
                "dropdown-menu",
                { "hidden": !isMenuOpen }
            )} aria-labelledby="dropdownMenuButton">
                <div className="list-group">
                    <a className="list-group-item" href="https://www.rosdistant.ru/" target="_blank" rel="noreferrer noopener">
                        <img alt='logo' src='https://tltsu.ru/bitrix/templates/tltsu_new/img/logo/rosdis.png'/>
                        Росдистант (абитуриент)
                    </a>
                    <a className="list-group-item" href="https://edu.rosdistant.ru/" target="_blank" rel="noreferrer noopener">
                        <img alt='logo' src='https://tltsu.ru/bitrix/templates/tltsu_new/img/logo/rosdis.png'/>
                        Росдистант (преподаватель, студент)
                    </a>
                    <a className="list-group-item" href="https://mail.tltsu.ru/mail/src/login.php" target="_blank" rel="noreferrer noopener">
                        <i className="fa fa-envelope-o fa-fw" aria-hidden="true"/>
                        Почта
                    </a>
                    <a className="list-group-item" href="http://edu.tltsu.ru/sites/site.php?s=196&m=26166" target="_blank" rel="noreferrer noopener">
                        <i className="fa fa-question-circle fa-fw" aria-hidden="true"/>
                        Вопрос ректору
                    </a>
                    <a className="list-group-item" href="http://cnit.tltsu.ru/sites/site.php?s=117&m=1101" target="_blank" rel="noreferrer noopener">
                        <i className="fa fa-info-circle fa-fw" aria-hidden="true"/>
                        АИСУ
                    </a>
                    <a className="list-group-item" href="http://cnit.tltsu.ru/sites/site.php?s=117&m=48453" target="_blank" rel="noreferrer noopener">
                        <i className="fa fa-life-ring fa-fw" aria-hidden="true"/>
                        Служба АХО
                    </a>
                    <a className="list-group-item" href="https://portal.tltsu.ru/" target="_blank" rel="noreferrer noopener">
                        <i className="fa fa-user-circle-o fa-fw" aria-hidden="true"/>
                        Портал Битрикс 24
                    </a>
                    <a className="list-group-item" href="http://edu.tltsu.ru/" target="_blank" rel="noreferrer noopener">
                        <i className="fa fa-key fa-fw" aria-hidden="true"/>
                        Образовательный портал
                    </a>
                    <a className="list-group-item" href="https://tltsu.ru/media-tsu/application/" target="_blank" rel="noreferrer noopener">
                        <img src='https://tltsu.ru/bitrix/templates/tltsu_new/img/logo/logo_mh.png' alt='logo'/>
                        Заявка на информационное сопровождение
                    </a>
                </div>
            </div>
            <button className="dropdown_button_adaptive" type="button"
                    onClick={() => changeMenuState(!isMenuOpen)}>
                <div className="img"/>
                <div className={classNames(
                    "fa fa-sort-asc style styleSetBut",
                    { "active": isMenuOpen }
                )}/>
            </button>
            {/*выше сервисы ТГУ*/}
        </div>
    );
}
