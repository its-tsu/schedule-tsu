import React from 'react';
import './style.scss';
import classNames from "classnames";

export interface DayClassEmptyProps {
    pairNumber: number
}

export default function DayClassEmpty({ pairNumber }: DayClassEmptyProps) {
    return(
        <article className={classNames(
            "day-schedule__list__item",
            "_empty"
        )}>
            <div className="time-info">
                <span className="time-info__pair-number">{pairNumber}</span>
            </div>
            <div className="info">
                <h3>Нет пары</h3>
            </div>
        </article>
    );
}
