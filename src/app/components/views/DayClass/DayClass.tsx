import React from 'react';
import './style.scss';
import classNames from "classnames";
import {ScheduleResponse} from "../../../models/response/ScheduleResponse";
import {DATES, STRINGS} from "../../../utils/functions";
import {typesColorsMap} from "../../../consts/types-colors";

export interface DayClassProps {
    scheduleList: Array<ScheduleResponse>,
    currentView: string,
    type: string,
}

export default function DayClass({scheduleList, currentView, type}: DayClassProps) {
    const typeClassName = (schedule: ScheduleResponse) => typesColorsMap.get(schedule.type) ? typesColorsMap.get(schedule.type) : "_default";

    return (
        <article className={classNames(
            "day-schedule__list__item",
            typeClassName(scheduleList[0])
        )}>
            <div className="time-info">
                <span className="time-info__pair-number">{scheduleList[0].pairNumber}</span>
                <span
                    className="time-info__time">{DATES.getPairTimeByDates(scheduleList[0].fromTime, scheduleList[0].toTime)}</span>
            </div>
            <div className="day-schedule__list__item__schedule-groups">
                {
                    scheduleList.map(schedule => (
                        <div className="info" key={schedule.id}>
                            <div className="info__title-groups">
                                <h3>{schedule.disciplineName} ({schedule.type})</h3>
                                <div className="info__title-groups__groups">
                                    {
                                        schedule.groupsList.map(group => (
                                            <p className={classNames(
                                                "info__title-groups__groups__item",
                                                {"_bold": currentView === group.name}
                                            )} key={group.id}>{group.name}</p>
                                        ))
                                    }
                                </div>
                            </div>
                            <div className="schedule-cell__info">
                                <div className="teacher-classroom-block">
                                    <p className="teacher-classroom-block__teacher">{!!schedule.teacher && STRINGS.getTeacherShortName(schedule.teacher)}</p>
                                    {
                                        !!schedule.classroom && <p className="teacher-classroom-block__classroom">
                                            {STRINGS.getClassroomName(schedule.classroom, schedule.note, type)}
                                        </p>
                                    }
                                </div>
                            </div>
                        </div>
                    ))
                }
            </div>
        </article>
    );
}
