import React, { useEffect, useState } from 'react';
import './style.scss';
import './adaptive.scss';
import Services from "../Services/Services";
import HeaderSearchField from '../../control/search-fields/HeaderSearchField/HeaderSearchField';
import HeaderNavigationMenu from '../HeaderNavigationMenu/HeaderNavigationMenu';
import { NavLink, useLocation, useHistory } from "react-router-dom";
import { PATHS } from "../../../utils/constants";
import Cookies from "cookies-ts";

const cookies = new Cookies();

export default function Header() {

    const location = useLocation();
    const history = useHistory();
    const path: string = location.pathname;
    const [title, setTitle] = useState("");
    const [rootPath, setRootPath] = useState("");

    useEffect(() => {
        setRootPath(location.pathname.includes(PATHS.terminalsRoot) ? PATHS.terminalsRoot + "/" : "/");
    }, [location.pathname]);

    function setTitleByUrl(path: string): void {

        if(~path.indexOf('students')) {
            setTitle("Расписание студентов");
            return;
        }

        if(~path.indexOf('teachers')) {
            setTitle("Расписание преподавателей");
            return;
        }

        if(~path.indexOf('classrooms')) {
            setTitle("Расписание аудиторий");
            return;
        }

        setTitle("");
    }

    useEffect(() => {
        setTitleByUrl(path);
    }, [path]);

    const logout = () => {
        cookies.remove("token");
        cookies.remove("role");
        history.push("/admin/login");
    };

    return(
        <header className="main-header">
            <section className="main-header__top">
                <div className="container center">
                    {
                        !rootPath || rootPath === "/"
                            ? <Services/>
                            : <button type="button" className="main-header__top__back-button" onClick={history.goBack}>
                                <i className="back-icon"/> Назад
                        </button>
                    }
                    <NavLink to={rootPath} className="title-link"><h1 className="main-header__top__title">Расписание ТГУ</h1></NavLink>
                    <div className="main-header__top__tools">
                        {
                            (!rootPath || rootPath === "/") && <HeaderSearchField/>
                        }
                        <HeaderNavigationMenu/>
                        {
                            cookies.get("token") &&
                            <button type="button" className="logout" onClick={logout}/>
                        }
                    </div>
                </div>
            </section>
            {
                title && <section className="main-header__bottom">
                    <h2 className="container center main-header__bottom__header">{title}</h2>
                </section>
            }
        </header>
    );
}
