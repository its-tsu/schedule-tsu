import React from 'react';
import './style.scss';
import './adaptive.scss';
import { withAdaptive } from 'react-adaptive';
import * as LogoTSU from './../../../media/images/logo-tsu.png';
import * as LogoTSUMobile from './../../../media/images/tsu-logo-mobile.png';
import * as LogoITS from './../../../media/images/logo-its.png';
import * as LogoITSMobile from './../../../media/images/its-logo-mobile.png';
import { WindowSize } from '../../../classes/other.classes';
import { BREAKPOINTS, LINKS } from '../../../utils/constants';

function Footer() {
    return (
        <footer className="main-footer">
            <div className="main-footer__top">
                <div className="container center">
                    <a href={LINKS.tsu} className="Link" target="_blank" rel="noopener noreferrer">
                        <img src={LogoTSU} alt="Логотип ТГУ"/>
                    </a>
                    <a href="#" target="_blank" rel="noopener noreferrer" className="logo-its">
                        <img src={LogoITS} alt="Логотип ITs"/>
                    </a>
                </div>
            </div>
            <div className="main-footer__bottom">
                © 2002 - 2021 опорный вуз Тольяттинский государственный университет. Все права защищены
            </div>
        </footer>
    );
}

function FooterTablet() {
    return (
        <footer className="main-footer">
            <div className="main-footer__top">
                <div className="container center">
                    <a href={LINKS.tsu} className="Link" target="_blank" rel="noopener noreferrer">
                        <img src={LogoTSU} alt="Логотип ТГУ"/>
                    </a>
                    <a href="#" target="_blank" rel="noopener noreferrer" className="logo-its">
                        <img src={LogoITS} alt="Логотип ITs"/>
                    </a>
                </div>
            </div>
            <div className="main-footer__bottom">
                © 2002 - 2021 опорный вуз Тольяттинский государственный университет.  Все права защищены
            </div>
        </footer>
    );
}

function FooterMobile() {
    return (
        <footer className="main-footer">
            <div className="main-footer__top">
                <div className="container center">
                    <a href={LINKS.tsu} className="Link" target="_blank" rel="noopener noreferrer">
                        <img src={LogoTSUMobile} alt="Логотип ТГУ"/>
                    </a>
                    <a href="#" target="_blank" rel="noopener noreferrer" className="logo-its">
                        <img src={LogoITSMobile} alt="Логотип ITs"/>
                    </a>
                </div>
            </div>
            <div className="main-footer__bottom">
                © 2002 - 2021 опорный вуз Тольяттинский государственный университет.  Все права защищены
            </div>
        </footer>
    );
}

const mapSizeToComponent = (size: WindowSize) => () => {

    if (size.width < BREAKPOINTS.portraitPhonesAdaptive) {
        return <FooterMobile/>;
    }

    if (size.width < BREAKPOINTS.landscapePhonesAdaptive) {
        return <FooterTablet/>;
    }

    return <Footer/>;
};

export default withAdaptive({ mapSizeToComponent });
