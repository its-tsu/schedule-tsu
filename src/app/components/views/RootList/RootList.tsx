import React, { useCallback, useContext, useEffect, useState } from 'react';
import './style.scss';
import { BaseModel } from "../../../models/BaseModel";
import { ApiService } from "../../../services/ApiService";
import { AppContext } from "../../../App";
import { NavLink, useLocation } from "react-router-dom";
import { PATHS } from "../../../utils/constants";

export interface InstitutesListProps {
    type: string
}

const apiService = new ApiService();

export default function RootList({ type }: InstitutesListProps) {

    const location = useLocation();
    const { addLoader, removeLoader, addErrorMessage } = useContext(AppContext);
    const addLoaderCallback = useCallback(addLoader, []);
    const removeLoaderCallback = useCallback(removeLoader, []);
    const addErrorMessageCallback = useCallback(addErrorMessage, []);
    
    const [rootPath, setRootPath] = useState("");

    const [rootItemsList, setRootItemsList] = useState<Array<BaseModel>>([]);

    useEffect(() => {
        addLoaderCallback("ROOT_LOADER");
        
        apiService.getRootItemsList(type)
            .then(itemsList => setRootItemsList(itemsList))
            .catch(() => addErrorMessageCallback("Ошибка! Не удалось получить список элементов"))
            .finally(() => removeLoaderCallback("ROOT_LOADER"));
    }, [addErrorMessageCallback, addLoaderCallback, removeLoaderCallback, type]);

    useEffect(() => {
        setRootPath(location.pathname.includes(PATHS.terminalsRoot) ? PATHS.terminalsRoot : "");
    }, [location.pathname]);

    return(
        <nav className={"institutes__links-list container-laptop _" + type}>
            {
                !!rootItemsList.length &&
                    rootItemsList.map((institute: BaseModel) => (
                        <NavLink to={`${rootPath}/${type}-groups/${institute.id}/${institute.name}`} key={institute.id}>{institute.name}</NavLink>
                    ))
            }
        </nav>
    );
}
