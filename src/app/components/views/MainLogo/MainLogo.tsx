import React from 'react';
import './style.scss';

import * as MainLogo from '../../../media/images/tsu-main-logo.png';

export default function MainLogoLink() {
    return(
        <img src={MainLogo} alt="Логотип ТГУ" className="main-logo"/>
    );
}
