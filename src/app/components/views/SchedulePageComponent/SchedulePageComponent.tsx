import React, {useState, useEffect, useContext, useCallback} from 'react';
import './style.scss';
import './print.scss';
import CalendarSlider from '../../control/calendar/CalendarSlider/CalendarSlider';
import moment, {Moment} from 'moment';
import {Day, Week} from '../../../classes/calendar.classes';
import FilterPanel from '../../control/FilterPanel/FilterPanel';
import {BaseModel} from "../../../models/BaseModel";
import {ScheduleResponse} from "../../../models/response/ScheduleResponse";
import ScheduleTable from "../ScheduleTable/ScheduleTable";
import {ApiService} from "../../../services/ApiService";
import {GroupResponse} from "../../../models/response/GroupResponse";
import {TeachersResponse} from "../../../models/response/TeachersResponse";
import {ClassroomResponse} from "../../../models/response/ClassroomResponse";
import {BaseModelConverter} from "../../../utils/converters/BaseModelConverter";
import {ARRAYS, URL} from "../../../utils/functions";
import {AppContext} from "../../../App";
import DaySchedule from "../DaySchedule/DaySchedule";
import {useHistory, useLocation} from 'react-router-dom';
import axios, {CancelTokenSource} from "axios";

const apiService = new ApiService();
const baseModelConverter = new BaseModelConverter();

interface ScheduleUrlParams {
    isWeek: boolean | string;
    selectedDate: string
}

export interface SchedulePageComponentProps {
    type: string;
    id: number;
}

const CancelToken = axios.CancelToken;
let source: CancelTokenSource | null;

export default function SchedulePageComponent({type, id}: SchedulePageComponentProps) {

    const history = useHistory();
    const location = useLocation();

    const {addLoader, removeLoader, addErrorMessage} = useContext(AppContext);
    const addLoaderCallback = useCallback(addLoader, []);
    const removeLoaderCallback = useCallback(removeLoader, []);
    const addErrorMessageCallback = useCallback(addErrorMessage, []);

    const [currentDate, changeDate] = useState<Moment>(moment());
    const [isWeek, setWeekState] = useState(true);
    const [currentRootItem, setRootItemState] = useState(new BaseModel(0, ''));
    const [currentCourse, setCourseState] = useState(new BaseModel(0, ''));
    const [currentView, setCurrentView] = useState<GroupResponse | TeachersResponse | ClassroomResponse | null>(null);
    const [scheduleList, setScheduleList] = useState<Array<ScheduleResponse>>([]);

    function changeUrl(isWeek: boolean, currentDate: string) {
        history.push({
            pathname: location.pathname,
            search: `?isWeek=${isWeek}&selectedDate=${currentDate}`
        });
    }

    function onDateClick(date: Week | Day) {
        const searchParams: ScheduleUrlParams = JSON.parse(URL.getJsonFromUrl(location.search));
        let selectedDate;

        if (date instanceof Week) {
            selectedDate = date.weekDays[0].date.toISOString();
        } else {
            selectedDate = date.date.toISOString();
        }

        changeUrl(Boolean(searchParams.isWeek), selectedDate);
    }

    function onTodayClick() {
        changeUrl(false, moment().toISOString());
    }

    function onWeekStateChange() {
        const searchParams: ScheduleUrlParams = JSON.parse(URL.getJsonFromUrl(location.search));

        changeUrl(!isWeek, searchParams.selectedDate);
    }

    function onFilterChange(setting: string, value: BaseModel) {
        switch (setting) {
            case "rootItem": {
                setRootItemState(value);
                addLoaderCallback("ROOT_ITEM_FILTER_CHANGE");

                apiService.getItemsList(type, value.id)
                    .then(res => {
                        const group = res.sort((a, b) => ARRAYS.sortByString(a.name, b.name))[0];

                        if (!group) {
                            window.location.reload();
                        }

                        setCourseState(group);

                        setPageByItemsList(type, value.id, group.id ? group.id : group.name, "ROOT_ITEM_FILTER_CHANGE");
                    })
                    .catch(() => addErrorMessageCallback("Не удалось загрузить расписание по выбранным параметрам."))
                    .finally(() => removeLoaderCallback("ROOT_ITEM_FILTER_CHANGE"));
                break;
            }
            case "groupItem": {
                setCourseState(value);
                addLoaderCallback("GROUP_ITEM_FILTER_CHANGE");
                setPageByItemsList(type, currentRootItem.id, value.id ? value.id : value.name, "GROUP_ITEM_FILTER_CHANGE");
                break;
            }
            case "item": {
                history.push({
                    pathname: `/${type}/${value.id}`,
                    search: location.search
                });
                break;
            }
            default:
                return;
        }
    }

    function setPageByItemsList(type: string, rootItemId: number, searchParam: number | string, loaderId: string) {

        apiService.getListItems(type, rootItemId, searchParam)
            .then(res => {
                const item = res.sort((a, b) => ARRAYS.sortByString(a.name, b.name))[0];

                if (res) {
                    history.push({
                        pathname: `/${type}/${item.id}`,
                        search: location.search
                    });
                } else {
                    window.location.reload();
                }
            })
            .catch(() => addErrorMessageCallback("Не удалось загрузить расписание по выбранным параметрам."))
            .finally(() => removeLoaderCallback(loaderId));
    }

    useEffect(() => {
        const searchParams: ScheduleUrlParams = JSON.parse(URL.getJsonFromUrl(location.search));
        setWeekState(Boolean(searchParams.isWeek));
        changeDate(moment(searchParams.selectedDate));
    }, [location.search]);

    useEffect(() => {
        addLoaderCallback("SCHEDULE_DOWNLOAD");
        addLoaderCallback("SCHEDULE_DOWNLOAD_2");

        if (source) {
            source.cancel();
        }

        source = CancelToken.source();

        apiService.getScheduleByTypeAndId(type, id, currentDate, isWeek, source)
            .then(res => setScheduleList(res))
            .catch(error => {
                if (error !== "canceled") {
                    addErrorMessageCallback("Ошибка! Не удалось загрузить расписание");
                }
            })
            .finally(() => removeLoaderCallback("SCHEDULE_DOWNLOAD"));

        apiService.getItemByIdAndType(id, type)
            .then(res => {
                if (res === null) return;
                setCurrentView(res);
                setRootItemState(baseModelConverter.getRootItem(res));
                setCourseState(baseModelConverter.getItemsGroup(res));
            })
            .catch(() => addErrorMessageCallback("Не удалось загрузить расписание по выбранным параметрам."))
            .finally(() => removeLoaderCallback("SCHEDULE_DOWNLOAD_2"));
    }, [addErrorMessageCallback, addLoaderCallback, currentDate, id, isWeek, removeLoaderCallback, type]);

    return (
        <section className="schedule-page__content container">
            <CalendarSlider isWeek={isWeek} onDateClick={onDateClick} currentDate={currentDate}/>
            <div className="separate-line"/>
            <main className="schedule-view">
                <FilterPanel currentRootItem={currentRootItem}
                             currentCourse={currentCourse}
                             currentItem={currentView}
                             isWeek={isWeek}
                             type={type}
                             onSettingChange={onFilterChange}
                             onWeekStateChange={onWeekStateChange}
                             onTodayClick={onTodayClick}
                />
                {
                    isWeek
                        ? <ScheduleTable type={type}
                                         scheduleList={scheduleList}
                                         currentView={currentView ? currentView.name : ""}
                                         date={currentDate}
                        />
                        : <DaySchedule type={type}
                                       scheduleList={scheduleList}
                                       currentView={currentView ? currentView.name : ""}
                                       date={currentDate}
                        />
                }
            </main>
            {
                isWeek && <button type="button" className="print-button" onClick={window.print}>
                    <i className="print-button__icon"/>
                    <span className="print-button__text">Печать</span>
                </button>
            }
        </section>
    );
}
