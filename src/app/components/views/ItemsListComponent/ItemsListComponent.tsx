import React, { useCallback, useContext, useEffect, useState } from 'react';
import './style.scss';
import DropdownList from '../../shared/DropdownList/DropdownList';
import { ARRAYS } from '../../../utils/functions';
import { BaseModel } from "../../../models/BaseModel";
import { ApiService } from "../../../services/ApiService";
import { AppContext } from "../../../App";
import ItemsList from "../../shared/ItemsList/ItemsList";
import {ScheduleTypes} from "../../../models/enums/ScheduleTypes";

const BACHELORS = "Бакалавриат";

export interface GroupsComponentProps {
    rootItemId: number;
    type: string;
}

const apiService = new ApiService();

export default function ItemsListComponent({ type, rootItemId }: GroupsComponentProps) {

    const { addLoader, removeLoader, addErrorMessage } = useContext(AppContext);
    const addLoaderCallback = useCallback(addLoader, []);
    const removeLoaderCallback = useCallback(removeLoader, []);

    const addErrorMessageCallback = useCallback(addErrorMessage, []);

    const [sectionsList, setSectionsList] = useState<Map<string, BaseModel[]>>(type === ScheduleTypes.GROUP ? new Map([[BACHELORS, []]]) : new Map());

    useEffect(() => {
        addLoaderCallback("ITEMS_LIST");
        
        apiService.getItemsList(type, rootItemId)
            .then(res => {
                res.forEach(item => {
                    if (item.name.endsWith('курс')) {
                        setSectionsList(sectionsList => {
                            const section = sectionsList.get(BACHELORS) || [];
                            section.push(item);
                            sectionsList.set(BACHELORS, section);
                            return sectionsList;
                        })
                    } else {
                        sectionsList.set(item.name, [new BaseModel(item.id, item.name)]);
                    }
                });
            })
            .catch(() => addErrorMessageCallback("Ошибка! Не удалось получить список элементов"))
            .finally(() => removeLoaderCallback("ITEMS_LIST"));
    }, [addErrorMessageCallback, addLoaderCallback, removeLoaderCallback, rootItemId, type]);

    return(
        <ul className={`groups-courses-list _${type}`}>
            {
                [...sectionsList.keys()].sort().map((key) => {
                    const list = (sectionsList.get(key) || []).sort((a, b) => ARRAYS.sortByString(a.name, b.name));

                    if (list.length === 1) {
                        const item = list[0];

                        return (
                            <DropdownList key={item.name} title={item.name}>
                                <ItemsList searchParam={item.id ? item.id : item.name} rootItemId={rootItemId} type={type}/>
                            </DropdownList>
                        )
                    }

                    return (
                        <DropdownList key={key} title={key}>
                            {
                                list.map((item, index) => (
                                    <DropdownList key={index} title={item.name}>
                                        <ItemsList searchParam={item.id ? item.id : item.name} rootItemId={rootItemId} type={type}/>
                                    </DropdownList>
                                ))
                            }
                        </DropdownList>
                    )
                })
            }
        </ul>
    );
}
