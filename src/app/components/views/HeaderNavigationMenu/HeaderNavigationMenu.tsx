import React, { useEffect, useRef, useState } from 'react';
import './style.scss';
import classNames from 'classnames';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { useClickOutside } from 'use-events';
import { PATHS } from '../../../utils/constants';
import { NavLink, useLocation } from 'react-router-dom';

export default function HeaderNavigationMenu() {

    const location = useLocation();
    const headerNavMenuRef = useRef(null);
    
    const [isWindowOpen, changeWindowState] = useState(false);
    const [rootPath, setRootPath] = useState("");

    useClickOutside([headerNavMenuRef], () => changeWindowState(false));

    useEffect(() => {
        setRootPath(location.pathname.includes(PATHS.terminalsRoot) ? PATHS.terminalsRoot : "");
    }, [location.pathname]);
    
    return(
        <section className="header-navigation-menu" ref={headerNavMenuRef}>
            <button className="header-navigation-menu__burger-menu" type="button" onClick={() => changeWindowState(!isWindowOpen)}>
                <svg className={classNames(
                    "ham hamRotate ham4",
                    { '_active': isWindowOpen }
                )} viewBox="0 0 100 100" width="35">
                    <path
                        className="line top"
                        d="m 70,33 h -40 c 0,0 -8.5,-0.149796 -8.5,8.5 0,8.649796 8.5,8.5 8.5,8.5 h 20 v -20"/>
                    <path
                        className="line middle"
                        d="m 70,50 h -40"/>
                    <path
                        className="line bottom"
                        d="m 30,67 h 40 c 0,0 8.5,0.149796 8.5,-8.5 0,-8.649796 -8.5,-8.5 -8.5,-8.5 h -20 v 20"/>
                </svg>
            </button>
            <TransitionGroup>
                {
                    isWindowOpen
                    && <CSSTransition timeout={300} classNames="nav-window">
                        <nav className="header-navigation-menu__navigation-list" onClick={() => changeWindowState(false)}>
                            <NavLink to={rootPath + PATHS.studentsInstitutesList}
                                     className="header-navigation-menu__navigation-list__item">Расписание студентов</NavLink>
                            <NavLink to={rootPath + PATHS.teachersInstitutesList}
                                     className="header-navigation-menu__navigation-list__item">Расписание преподавателей</NavLink>
                            <NavLink to={rootPath + PATHS.classroomsInstitutesList}
                                     className="header-navigation-menu__navigation-list__item">Расписание аудиторий</NavLink>
                            {/*<NavLink to={PATHS.testsCenterInstitutesList}*/}
                            {/*   className="header-navigation-menu__navigation-list__item">Расписание центра тестирования</A>*/}
                        </nav>
                    </CSSTransition>
                }
            </TransitionGroup>
        </section>
    );
}
