import { AxiosPromise } from "axios";
import Cookies from "cookies-ts";
import client, { admin } from "./axios.config";

const cookies = new Cookies();

export function login(login: string, password: string): AxiosPromise {
    return client.post("/login", { name: login, password });
}

export function hideClassroom(id: number) {
    return admin.post("/classrooms", id, {
        headers: {
            Authorization: "Bearer " + cookies.get("token"),
            'Content-Type': 'application/json'
        }
    });
}

export function hideDepartment(id: number) {
    return admin.post("/departments", id, {
        headers: {
            Authorization: "Bearer " + cookies.get("token"),
            'Content-Type': 'application/json'
        }
    });
}

export function hideGroup(id: number) {
    return admin.post("/groups", id, {
        headers: {
            Authorization: "Bearer " + cookies.get("token"),
            'Content-Type': 'application/json'
        }
    });
}

export function hideInstitute(id: number) {
    return admin.post("/institutes", id, {
        headers: {
            Authorization: "Bearer " + cookies.get("token"),
            'Content-Type': 'application/json'
        }
    });
}

export function hideTeacher(id: number) {
    return admin.post("/teachers", id, {
        headers: {
            Authorization: "Bearer " + cookies.get("token"),
            'Content-Type': 'application/json'
        }
    });
}

export function showClassroom(id: number) {
    return admin.delete("/classrooms/" + id, {
        headers: {
            Authorization: "Bearer " + cookies.get("token")
        }
    });
}

export function showDepartment(id: number) {
    return admin.delete("/departments/" + id, {
        headers: {
            Authorization: "Bearer " + cookies.get("token")
        }
    });
}

export function showGroup(id: number) {
    return admin.post("/groups/" + id, {
        headers: {
            Authorization: "Bearer " + cookies.get("token")
        }
    });
}

export function showInstitute(id: number) {
    return admin.post("/institutes/" + id, {
        headers: {
            Authorization: "Bearer " + cookies.get("token")
        }
    });
}

export function showTeacher(id: number) {
    return admin.post("/teachers/" + id, {
        headers: {
            Authorization: "Bearer " + cookies.get("token")
        }
    });
}

