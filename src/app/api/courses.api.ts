import client from "./axios.config";

export async function getCoursesList(instId: number): Promise<string[]> {
    try {
        const { data } = await client.get(`/courses/institute/${instId}`);
        return data;
    } catch (e) {
        console.error(e);
        throw e;
    }
}