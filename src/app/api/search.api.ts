import client from './axios.config';
import { FormattedItem } from "../classes/search.classes";
import { SearchResponse } from "../models/response/SearchResponse";
import { CancelTokenSource } from "axios";

function randomiseResponse(searchList: Array<FormattedItem>): SearchResponse {
    return searchList[Math.floor(Math.random() * searchList.length)];
}

export async function getScheduleListBySearchParam(searchParam: string, source: CancelTokenSource): Promise<SearchResponse[]> {
    try {
        const { data } = await client.get(`/search?name=${searchParam}`, { cancelToken: source.token });
        return data;
    } catch (e) {
        console.error(e);
        throw e;
    }
}

export async function getRandomSchedule(): Promise<SearchResponse | null> {
    try {
        const { data } = await client.get(`/search?name=`);
        return randomiseResponse(data);
    } catch (e) {
        console.error(e);
        throw e;
    }
}