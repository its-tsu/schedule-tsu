import client from "./axios.config";
import { ScheduleResponse } from "../models/response/ScheduleResponse";
import { CancelTokenSource } from "axios";

export async function getScheduleForGroup(groupId: number, fromDate: string, toDate: string, source: CancelTokenSource): Promise<ScheduleResponse[]> {
    try {
        const { data } = await client.get(`/schedule/group?groupId=${groupId}&fromDate=${fromDate}&toDate=${toDate}`, {
            cancelToken: source.token
        });
        return data;
    } catch (e) {
        console.error(e);
        throw e;
    }
}

export async function getScheduleForTeacher(teacherId: number, fromDate: string, toDate: string, source: CancelTokenSource): Promise<ScheduleResponse[]> {
    try {
        const { data } = await client.get(`/schedule/teacher?teacherId=${teacherId}&fromDate=${fromDate}&toDate=${toDate}`, {
            cancelToken: source.token
        });
        return data;
    } catch (e) {
        console.error(e);
        throw e;
    }
}

export async function getScheduleForClassroom(classroomId: number, fromDate: string, toDate: string, source: CancelTokenSource): Promise<ScheduleResponse[]> {
    try {
        const { data } = await client.get(`/schedule/classroom?classroomId=${classroomId}&fromDate=${fromDate}&toDate=${toDate}`, {
            cancelToken: source.token
        });
        return data;
    } catch (e) {
        console.error(e);
        throw e;
    }
}