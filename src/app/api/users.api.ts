import { admin } from "./axios.config";
import Cookies from "cookies-ts";
import { UserResponse } from "../models/response/UserResponse";

const cookies = new Cookies();

export function getAllUsers() {
    return admin.get("/users", {
        headers: {
            Authorization: "Bearer " + cookies.get("token")
        }
    });
}

export function createUserRequest({
                               name,
                               login,
                               role,
                               password
                           }: UserResponse) {
    return admin.post("/users", { name, login, role, password }, {
        headers: {
            Authorization: "Bearer " + cookies.get("token")
        }
    });
}

export function deleteUserById(id: number) {
    return admin.delete("/users/" + id, {
        headers: {
            Authorization: "Bearer " + cookies.get("token")
        }
    });
}