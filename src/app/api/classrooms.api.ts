import client, { admin } from './axios.config';
import Cookies from "cookies-ts";
import { BaseModel } from "../models/BaseModel";
import {ClassroomResponse} from "../models/response/ClassroomResponse";

const cookies = new Cookies();

export async function getClassroomsByBuildingAndFloor(buildingId: number, floorNumber: string): Promise<ClassroomResponse[]> {
    try {
        const { data } = await client.get(`/classrooms/building/${buildingId}/floor/${floorNumber}`);
        return data;
    } catch (e) {
        console.error(e);
        throw e;
    }
}

export async function getFloorsByBuildingId(buildingId: number): Promise<string[]> {
    try {
        const { data } = await client.get(`/floors/building/${buildingId}`);
        return data;
    } catch (e) {
        console.error(e);
        throw e;
    }
}

export async function getBuildingsList(): Promise<BaseModel[]> {
    try {
        const { data } = await client.get(`/buildings`);
        return data;
    } catch (e) {
        console.error(e);
        throw e;
    }
}

export async function getClassroomById(classroomId: number) {
    try {
        const { data } = await client.get("/classrooms/" + classroomId);
        return data;
    } catch (e) {
        console.error(e);
        throw e;
    }
}

export async function getAllClassrooms(): Promise<BaseModel[]> {
    try {
        const { data } = await client.get("/classrooms");
        return data;
    } catch (e) {
        console.error(e);
        throw e;
    }
}

export async function getHiddenClassrooms(): Promise<BaseModel[]> {
    try {
        const { data } = await admin.get("/classrooms", {
            headers: {
                Authorization: "Bearer " + cookies.get("token")
            }
        });
        return data;
    } catch (e) {
        console.error(e);
        throw e;
    }
}