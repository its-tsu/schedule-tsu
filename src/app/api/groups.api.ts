import client, { admin } from './axios.config';
import Cookies from "cookies-ts";
import { GroupResponse } from "../models/response/GroupResponse";
import { BaseModel } from "../models/BaseModel";

const cookies = new Cookies();

export async function getGroupsListByInstIdAndCourseName(instId: number, courseName: string): Promise<GroupResponse[]> {
    try {
        const { data } = await client.get(`/groups/course/${courseName}/institute/${instId}`);
        return data;
    } catch (e) {
        console.error(e);
        throw e;
    }
}

export async function getGroupById(groupId: number): Promise<GroupResponse | null> {
    try {
        const { data } = await client.get("/groups/" + groupId);
        return data;
    } catch (e) {
        console.error(e);
        throw e;
    }
}

export async function getAllGroups(): Promise<GroupResponse[]> {
    try {
        const { data } = await client.get("/groups");
        return data;
    } catch (e) {
        console.error(e);
        throw e;
    }
}

export async function getHiddenGroups(): Promise<BaseModel[]> {
    try {
        const { data } = await admin.get("/groups", {
            headers: {
                Authorization: "Bearer " + cookies.get("token")
            }
        });
        return data;
    } catch (e) {
        console.error(e);
        throw e;
    }
}