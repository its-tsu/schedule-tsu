import client, { admin } from './axios.config';
import Cookies from "cookies-ts";
import { InstituteResponse } from "../models/response/InstituteResponse";
import { BaseModel } from "../models/BaseModel";

const cookies = new Cookies();

export async function getInstitutesList(): Promise<InstituteResponse[]> {
    try {
        const { data } = await client.get(`/institutes`);
        return data;
    } catch (e) {
        throw e;
    }
}

export async function getHiddenInstitutesList(): Promise<BaseModel[]> {
    try {
        const { data } = await admin.get(`/institutes`, {
            headers: {
                Authorization: "Bearer " + cookies.get("token")
            }
        });
        return data;
    } catch (e) {
        throw e;
    }
}