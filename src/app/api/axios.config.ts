import axios from 'axios';
import Cookies from "cookies-ts";

const cookies = new Cookies();
//const apiRoot = "http://localhost:8080/api/";
const apiRoot = "https://its.tltsu.ru/api/";

const client = axios.create({
    baseURL: apiRoot,
});

export const admin = axios.create({
    baseURL: apiRoot + "/admin",
});

client.interceptors.response.use((response) => {
    return response;
}, (error) => {
    if (axios.isCancel(error)) {
        return Promise.reject("canceled");
    }

    return Promise.reject(error.response.message || "Ошибка запроса");
});

admin.interceptors.response.use((response) => {
    return response;
}, (error) => {
    if (error.response.status === 401) {
        cookies.remove("token");
        cookies.remove("role");
        window.location.hash = "/admin/login";
    }

    if (axios.isCancel(error)) {
        return Promise.reject("canceled");
    }

    return Promise.reject(error.response.message || "Ошибка запроса");
});

export default client;

export const READ_TOKEN_PARAM = 'token=5EiXMVQlSZSCIHi1FlC8oBK644kkxccy';