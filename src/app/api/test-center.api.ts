import client, { READ_TOKEN_PARAM } from './axios.config';
import { formatTestCenterResponseArray } from '../utils/response-arrays';

export function getTestCenterList() {
    return client.get(`/Prepods.json?${READ_TOKEN_PARAM}&prepodKafId=2`)
        .then(res => formatTestCenterResponseArray(res.data.prepods))
        .catch(err => err);
}