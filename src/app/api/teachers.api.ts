import client, { admin } from './axios.config';
import Cookies from "cookies-ts";
import { DepartmentResponse } from "../models/response/DepartmentResponse";
import { TeachersResponse } from "../models/response/TeachersResponse";
import { BaseModel } from "../models/BaseModel";

const cookies = new Cookies();

export async function getTeacherDepartmentsByInstId(instId: number): Promise<DepartmentResponse[]> {
    try {
        const { data } = await client.get(`/departments/institute/${instId}`);
        return data;
    } catch (e) {
        console.error(e);
        throw e;
    }
}

export async function getAllDepartments(): Promise<DepartmentResponse[]> {
    try {
        const { data } = await client.get(`/departments`);
        return data;
    } catch (e) {
        console.error(e);
        throw e;
    }
}

export async function getHiddenDepartments(): Promise<BaseModel[]> {
    try {
        const { data } = await admin.get(`/departments`, {
            headers: {
                Authorization: "Bearer " + cookies.get("token")
            }
        });
        return data;
    } catch (e) {
        console.error(e);
        throw e;
    }
}

export async function getTeachersByDepartment(departmentId: number): Promise<TeachersResponse[]> {
    try {
        const { data } = await client.get(`/teachers/department/${departmentId}`);
        return data;
    } catch (e) {
        console.error(e);
        throw e;
    }
}

export async function getTeacherById(teacherId: number): Promise<TeachersResponse | null> {
    try {
        const { data } = await client.get("/teachers/" + teacherId);
        return data;
    } catch (e) {
        console.error(e);
        throw e;
    }
}

export async function getAllTeachers(): Promise<TeachersResponse[]> {
    try {
        const { data } = await client.get("/teachers");
        return data;
    } catch (e) {
        console.error(e);
        throw e;
    }
}

export async function getHiddenTeachers(): Promise<TeachersResponse[]> {
    try {
        const { data } = await admin.get("/teachers", {
            headers: {
                Authorization: "Bearer " + cookies.get("token")
            }
        });
        return data;
    } catch (e) {
        console.error(e);
        throw e;
    }
}