import { admin } from "./axios.config";
import Cookies from "cookies-ts";

const cookies = new Cookies();

export function executeHardUpdate() {
    return admin.post("/execute-hard-update", {}, {
        headers: {
            Authorization: "Bearer " + cookies.get("token")
        }
    });
}

export function executeLightUpdate() {
    return admin.post("/execute-light-update", {}, {
        headers: {
            Authorization: "Bearer " + cookies.get("token")
        }
    });
}

export function recreateSystemTables() {
    return admin.post("/recreate-system-tables", {}, {
        headers: {
            Authorization: "Bearer " + cookies.get("token")
        }
    });
}

export function recreateUserTable() {
    return admin.post("/recreate-user-table", {}, {
        headers: {
            Authorization: "Bearer " + cookies.get("token")
        }
    });
}