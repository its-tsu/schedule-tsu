import nanoid from 'nanoid';
import moment, { Moment } from 'moment';
import { DATES } from '../utils/functions';
import { createRef, RefObject } from 'react';

export class Day {
    id: string;
    date: Moment;
    dateNumber: number;
    dayName: string;

    constructor(date: Moment) {
        this.id = nanoid(8);
        this.date = moment(date);
        this.dateNumber = Number(this.date.format("D"));
        this.dayName = DATES.daysOfWeek[Number(this.date.format("E")) - 1];
    }
}

export class Week {
    id: string;
    weekDays: Array<Day>;
    elementRef: RefObject<HTMLLIElement>;

    constructor(weekDays: Array<Day>) {
        this.id = nanoid(8);
        this.weekDays = weekDays;
        this.elementRef = createRef();
    }

}