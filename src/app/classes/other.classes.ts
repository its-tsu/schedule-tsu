export class WindowSize {
    width: number;
    height: number;

    constructor(width: number, height: number) {
        this.width = width;
        this.height = height;
    }

}

export class UrlParams {
    instId?: number;
    instName?: string;
}