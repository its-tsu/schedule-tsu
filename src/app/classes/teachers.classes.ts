export class Teacher {
    prepodId: number;
    prepodName: string;

    constructor(id: number, name: string) {
        this.prepodId = id;
        this.prepodName = name;
    }
}