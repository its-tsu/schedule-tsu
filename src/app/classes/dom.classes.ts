export class ClientRect {
    width: number;
    height: number;
    x: number;
    y: number;
    right: number;
    left: number;
    top: number;
    bottom: number;

    constructor() {
        this.width = 0;
        this.height = 0;
        this.x = 0;
        this.y = 0;
        this.right = 0;
        this.left = 0;
        this.top = 0;
        this.bottom = 0;
    }
}