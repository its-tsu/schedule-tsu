export enum ErrorsEnum {
    NOT_FOUND = 404,
    SERVER_ERROR = 503
}