const typesColorsValues = [
    { value: "Пр", text: "_pr" },
    { value: "Лек", text: "_lec" },
    { value: "Лаб", text: "_lab" },
    { value: "Тест", text: "_test" },
    { value: "Экз", text: "_exam" },
    { value: "Конс", text: "_cons" },
    { value: "Веб", text: "_web" },
    { value: "Зач", text: "_zach" },
    { value: "Пересдача", text: "_per" },
];

export const typesColorsMap = new Map(
    typesColorsValues.map(typeColor => [typeColor.value, typeColor.text] as [string, string])
);