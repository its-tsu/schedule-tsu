import React, { lazy, Suspense } from 'react';
import MainPage from './pages/MainPage/MainPage';
import { Redirect, Route, Switch } from "react-router-dom";
import { PATHS } from './utils/constants';
import RootListPage from './pages/RootListPage/RootListPage';
import ItemsListPage from './pages/ItemsListPage/ItemsListPage';
import SchedulePage from './pages/SchedulePage/SchedulePage';
import { ScheduleTypes } from "./models/enums/ScheduleTypes";
import RootLoader from "./components/shared/RootLoader/RootLoader";
import ErrorPage from "./pages/ErrorPage/ErrorPage";
import { ErrorsEnum } from "./consts/errors.enum";

const AdminLogin = lazy(() => import("./pages/AdminSection/AdminLogin/AdminLogin"));
const AdminEntities = lazy(() => import("./pages/AdminSection/AdminEntities/AdminEntities"));
const AdminMain = lazy(() => import("./pages/AdminSection/AdminMain/AdminMain"));
const AdminConfiguration = lazy(() => import("./pages/AdminSection/AdminConfiguration/AdminConfiguration"));
const AdminUsers = lazy(() => import("./pages/AdminSection/AdminUsers/AdminUsers"));

export const Routes = () => (
    <Suspense fallback={RootLoader}>
        <Switch>
            <Route exact path={[
                PATHS.root,
                PATHS.terminalsRoot
            ]} component={MainPage}/>
            <Route exact path={[
                PATHS.studentsInstitutesList,
                PATHS.terminalsRoot + PATHS.studentsInstitutesList
            ]} render={() => <RootListPage type={ScheduleTypes.GROUP}/>}/>
            <Route exact path={[
                PATHS.teachersInstitutesList,
                PATHS.terminalsRoot + PATHS.teachersInstitutesList
            ]} render={() => <RootListPage type={ScheduleTypes.TEACHER}/>}/>
            <Route exact path={[
                PATHS.classroomsInstitutesList,
                PATHS.terminalsRoot + PATHS.classroomsInstitutesList
            ]} render={() => <RootListPage type={ScheduleTypes.CLASSROOM}/>}/>
            <Route exact path={[
                PATHS.studentsGroupsList,
                PATHS.terminalsRoot + PATHS.studentsGroupsList
            ]} render={({ match: { params } }) =>
                <ItemsListPage type={ScheduleTypes.GROUP} rootItemId={params.instId || 0} rootItemName={params.instName || ""}/>}
            />
            <Route exact path={[
                PATHS.teachersGroupsList,
                PATHS.terminalsRoot + PATHS.teachersGroupsList
            ]} render={({ match: { params } }) =>
                <ItemsListPage type={ScheduleTypes.TEACHER} rootItemId={params.instId || 0} rootItemName={params.instName || ""}/>}
            />
            <Route exact path={[
                PATHS.classroomsGroupsList,
                PATHS.terminalsRoot + PATHS.classroomsGroupsList
            ]} render={({ match: { params } }) =>
                <ItemsListPage type={ScheduleTypes.CLASSROOM} rootItemId={params.instId || 0} rootItemName={params.instName || ""}/>}
            />
            <Route exact path={[
                PATHS.groupSchedule,
                PATHS.terminalsRoot + PATHS.groupSchedule
            ]} render={({ match: { params } }) => <SchedulePage type={ScheduleTypes.GROUP} id={params.id}/>}/>
            <Route exact path={[
                PATHS.teacherSchedule,
                PATHS.terminalsRoot + PATHS.teacherSchedule
            ]} render={({ match: { params } }) => <SchedulePage type={ScheduleTypes.TEACHER} id={params.id}/>}/>
            <Route exact path={[
                PATHS.classroomSchedule,
                PATHS.terminalsRoot + PATHS.classroomSchedule
            ]} render={({ match: { params } }) => <SchedulePage type={ScheduleTypes.CLASSROOM} id={params.id}/>}/>
            <Route exact path={PATHS.adminRoot} component={AdminMain}/>
            <Route exact path={PATHS.adminEntities} component={AdminEntities}/>
            <Route exact path={PATHS.adminConfiguration} component={AdminConfiguration}/>
            <Route exact path={PATHS.adminUsers} component={AdminUsers}/>
            <Route exact path={PATHS.adminRoot + PATHS.adminLogin} component={AdminLogin}/>
            <Route exact path={PATHS.error404} render={() => <ErrorPage errorType={ErrorsEnum.NOT_FOUND}/>}/>
            <Redirect to="/error-404"/>
        </Switch>
    </Suspense>
);