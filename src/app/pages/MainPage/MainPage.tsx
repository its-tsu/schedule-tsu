import React, { useEffect, useState } from 'react';
import './style.scss';
import './adaptive.scss';
import MainLogo from '../../components/views/MainLogo/MainLogo';
import SearchComponent from '../../components/shared/SearchComponent/SearchComponent';
import { getRandomSchedule } from '../../api/search.api';
import { PATHS } from '../../utils/constants';
import { NavLink, useLocation } from "react-router-dom";
import { BROWSER } from "../../utils/functions";
import moment from "moment";

export default function MainPage() {

    const location = useLocation();
    const [example, setExample] = useState({ id: 0, name: '', type: "" });
    const [rootPath, setRootPath] = useState("");

    const currentDate = moment();
    const searchParamDate = currentDate.day() !== 0 ? currentDate.toISOString() : currentDate.add(1, 'days').toISOString();

    useEffect(() => {
        getRandomSchedule()
            .then(res => {
                if (res) {
                    setExample(res);
                }
            })
            .catch(err => console.error(err));
    }, []);

    useEffect(() => {
        setRootPath(location.pathname.includes(PATHS.terminalsRoot) ? PATHS.terminalsRoot : "");
    }, [location.pathname]);

    return (
        <main className="main-page">
            <div className="main-page__content container center">
                <MainLogo/>
                <h2 className="main-page__title">Расписание ТГУ</h2>
                <SearchComponent className="main-page__search"/>
                <div className="main-page__example">
                    <p>Например,</p>
                    <NavLink to={`${rootPath}/${example.type}/${example.id}?isWeek=${!BROWSER.isMobile()}&selectedDate=${searchParamDate}`}>{example.name}</NavLink>
                </div>
                <nav className="main-page__links">
                    <NavLink to={rootPath + PATHS.studentsInstitutesList} className="main-page__links__link">
                        <div className="main-page__links__link__icon students-icon"/>
                        <h3 className="main-page__links__link__title">Студенты</h3>
                    </NavLink>
                    <NavLink to={rootPath + PATHS.teachersInstitutesList} className="main-page__links__link">
                        <div className="main-page__links__link__icon teachers-icon"/>
                        <h3 className="main-page__links__link__title">Преподаватели</h3>
                    </NavLink>
                    <NavLink to={rootPath + PATHS.classroomsInstitutesList} className="main-page__links__link">
                        <div className="main-page__links__link__icon classrooms-icon"/>
                        <h3 className="main-page__links__link__title">Аудитории</h3>
                    </NavLink>
                    {/*<A href={PATHS.testsCenterInstitutesList} className="main-page__links__link">*/}
                    {/*    <div className="main-page__links__link__icon tests-center-icon"/>*/}
                    {/*    <h3 className="main-page__links__link__title">Центр тестирования</h3>*/}
                    {/*</NavLink>*/}
                </nav>
            </div>
        </main>
    );
}
