import React, { useEffect } from 'react';
import './style.scss';
import './adaptive.scss';
import MainLogoLink from '../../components/views/MainLogo/MainLogo';
import RootList from '../../components/views/RootList/RootList';
import { ScheduleTypes } from "../../models/enums/ScheduleTypes";

export interface RootListPageProps {
    type: string
}

export default function RootListPage({ type }: RootListPageProps) {

    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    return(
        <section className="institutes-page">
            <MainLogoLink/>
            <h2 className="institutes-page__title">{ type === ScheduleTypes.CLASSROOM ? "Корпусы" : "Институты" }</h2>
            <RootList type={type}/>
        </section>
    );
}
