import React, { useEffect } from 'react';
import './style.scss';
import ItemsListComponent from '../../components/views/ItemsListComponent/ItemsListComponent';

export interface ItemsListPageProps {
    rootItemId: number;
    rootItemName: string;
    type: string;
}

export default function ItemsListPage({ type, rootItemId, rootItemName }: ItemsListPageProps) {
    
    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);
    
    return(
        <section className="groups-page">
            <h2 className="groups-page__title">{decodeURIComponent(rootItemName)}</h2>
            <ItemsListComponent type={type} rootItemId={rootItemId}/>
        </section>
    );
}
