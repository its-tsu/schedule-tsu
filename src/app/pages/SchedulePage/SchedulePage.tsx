import React, { useEffect } from 'react';
import './style.scss';
import 'moment/locale/ru';
import SchedulePageComponent from '../../components/views/SchedulePageComponent/SchedulePageComponent';

export interface SchedulePageProps {
    type: string;
    id: number;
}

export default function SchedulePage({ type, id }: SchedulePageProps) {

    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    return(
        <section className="schedule-page">
            <SchedulePageComponent type={type} id={id}/>
        </section>
    );
}