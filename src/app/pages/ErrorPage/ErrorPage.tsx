import React from 'react';
import './style.scss';
import { ErrorsEnum } from "../../consts/errors.enum";
import { NavLink } from "react-router-dom";

export interface ErrorPageProps {
    errorType: ErrorsEnum
}

export default function ErrorPage({ errorType }: ErrorPageProps) {
    return(
        <div className="error-page">
            <p className="error-page__message">{
                errorType === ErrorsEnum.NOT_FOUND ? "Страница не найдена" : "Ошибка сервера"
            }</p>
            {
                errorType === ErrorsEnum.NOT_FOUND && <NavLink to="/" className="error-page__link">На Главную</NavLink>
            }
        </div>
    );
}
