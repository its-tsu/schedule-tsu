import { getAllGroups, getHiddenGroups } from "../../../api/groups.api";
import { getAllDepartments, getAllTeachers, getHiddenDepartments, getHiddenTeachers } from "../../../api/teachers.api";
import { getAllClassrooms, getHiddenClassrooms } from "../../../api/classrooms.api";
import { getHiddenInstitutesList, getInstitutesList } from "../../../api/institutes.api";
import { BaseModel } from "../../../models/BaseModel";
import {
    hideClassroom, hideDepartment, hideGroup, hideInstitute,
    hideTeacher,
    showClassroom,
    showDepartment, showGroup,
    showInstitute,
    showTeacher
} from "../../../api/admin.api";

export enum ItemsIds {
    GROUPS_ID,
    TEACHERS_ID,
    CLASSROOMS_ID,
    INSTITUTES_ID,
    DEPARTMENTS_ID,
}

export async function getAdminItemsList(type: number, hiddenItems: boolean): Promise<BaseModel[]> {

    if (hiddenItems) {
        switch (type) {
            case ItemsIds.TEACHERS_ID: {
                const response = await getHiddenTeachers();
                return response.map(item => new BaseModel(item.id, `${item.lastName} ${item.name[0]}.${item.patronymic[0]}.`));
            }
            case ItemsIds.CLASSROOMS_ID: {
                return await getHiddenClassrooms();
            }
            case ItemsIds.INSTITUTES_ID: {
                return await getHiddenInstitutesList();
            }
            case ItemsIds.DEPARTMENTS_ID: {
                return await getHiddenDepartments();
            }
            default: {
                return await getHiddenGroups();
            }
        }
    }

    switch (type) {
        case ItemsIds.TEACHERS_ID: {
            const response = await getAllTeachers();
            return response.map(item => new BaseModel(item.id, `${item.lastName} ${item.name[0]}.${item.patronymic[0]}.`));
        }
        case ItemsIds.CLASSROOMS_ID: {
            return await getAllClassrooms();
        }
        case ItemsIds.INSTITUTES_ID: {
            return await getInstitutesList();
        }
        case ItemsIds.DEPARTMENTS_ID: {
            const response = await getAllDepartments();
            return response.map(item => new BaseModel(item.id, item.name));
        }
        default: {
            const response = await getAllGroups();
            return response.map(item => new BaseModel(item.id, item.name));
        }
    }
}

export async function showItem(itemId: number, type: number) {
    switch (type) {
        case ItemsIds.GROUPS_ID: {
            await showGroup(itemId);
            break;
        }
        case ItemsIds.TEACHERS_ID: {
            await showTeacher(itemId);
            break;
        }
        case ItemsIds.CLASSROOMS_ID: {
            await showClassroom(itemId);
            break;
        }
        case ItemsIds.INSTITUTES_ID: {
            await showInstitute(itemId);
            break;
        }
        case ItemsIds.DEPARTMENTS_ID: {
            await showDepartment(itemId);
            break;
        }
        default: break;
    }
}

export async function hideItem(itemId: number, type: number) {
    switch (type) {
        case ItemsIds.GROUPS_ID: {
            await hideGroup(itemId);
            break;
        }
        case ItemsIds.TEACHERS_ID: {
            await hideTeacher(itemId);
            break;
        }
        case ItemsIds.CLASSROOMS_ID: {
            await hideClassroom(itemId);
            break;
        }
        case ItemsIds.INSTITUTES_ID: {
            await hideInstitute(itemId);
            break;
        }
        case ItemsIds.DEPARTMENTS_ID: {
            await hideDepartment(itemId);
            break;
        }
        default: break;
    }
}