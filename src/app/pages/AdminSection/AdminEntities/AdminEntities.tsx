import React, { useContext, useEffect, useState } from 'react';
import './style.scss';
import { BaseModel } from "../../../models/BaseModel";
import { getAdminItemsList, hideItem, ItemsIds, showItem } from "./admin.service";
import * as classNames from "classnames";
import { ARRAYS } from "../../../utils/functions";
import AdminWrapper from "../../../components/shared/AdminWrapper/AdminWrapper";
import EntitiesFilter from "../../../components/control/EntitiesFilter/EntitiesFilter";
import { AppContext } from "../../../App";

const itemsList = [
    new BaseModel(ItemsIds.GROUPS_ID, "Группы"),
    new BaseModel(ItemsIds.TEACHERS_ID, "Преподаватели"),
    new BaseModel(ItemsIds.CLASSROOMS_ID, "Аудитории"),
    new BaseModel(ItemsIds.INSTITUTES_ID, "Институты"),
    new BaseModel(ItemsIds.DEPARTMENTS_ID, "Кафедры")
];

const ADMIN_LOADER = "ADMIN_LOADER";

export default function AdminEntities() {
    let inputRef: HTMLInputElement | null = null;

    const { addLoader, removeLoader, addErrorMessage } = useContext(AppContext);

    const [currentItem, setCurrentItem] = useState(itemsList[0]);
    const [showHiddenItems, setFilter] = useState(false);
    const [adminItemsList, setAdminItems] = useState<BaseModel[]>([]);
    const [filterString, setFilterString] = useState("");
    const [checkedItemId, setCheckedItemId] = useState<number>(0);

    useEffect(() => {
        if (inputRef) {
            inputRef.value = "";
        }

        getAdminItemsList(currentItem.id, showHiddenItems)
            .then(res => setAdminItems(res))
            .catch(err => console.error(err))
            .finally();
    }, [currentItem, inputRef, showHiddenItems]);

    const changeItemState = async () => {
        addLoader(ADMIN_LOADER);

        try {
            if (showHiddenItems) {
                await showItem(checkedItemId, currentItem.id);
            } else {
                await hideItem(checkedItemId, currentItem.id);
            }

            const response = await getAdminItemsList(currentItem.id, showHiddenItems);
            setAdminItems(response);
        } catch (e) {
            addErrorMessage(e.data.message);
        } finally {
            removeLoader(ADMIN_LOADER);
        }
    };

    return(
        <AdminWrapper>
            <section className="admin-entities">
                <EntitiesFilter
                    currentItem={currentItem}
                    itemsList={itemsList}
                    isHidden={showHiddenItems}
                    inputRef={ref => inputRef = ref}
                    onSearch={setFilterString}
                    onItemSelect={setCurrentItem}
                    onHiddenChange={setFilter}
                />
                <section className="items-list">
                    {
                        adminItemsList.filter(it => it.name.toLowerCase().includes(filterString.toLowerCase()))
                            .sort((a, b) => ARRAYS.sortByString(a.name, b.name)).map(item => (
                            <label className={classNames(
                                "items-list__item",
                                { "_department": currentItem.id === ItemsIds.DEPARTMENTS_ID }
                            )}>
                                <input type="radio" onChange={() => setCheckedItemId(item.id)} checked={item.id === checkedItemId}/>
                                {item.name}
                                <div className="check">
                                    { item.id === checkedItemId && <div className="checked-icon"/> }
                                </div>
                            </label>
                        ))
                    }
                </section>
                <button type="button" className="show-hide-button" onClick={changeItemState}>{showHiddenItems ? "Отобразить выбранное" : "Скрыть выбранное"}</button>
            </section>
        </AdminWrapper>
    );
}
