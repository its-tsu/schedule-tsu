import React, { useContext } from 'react';
import './style.scss';
import AdminWrapper from "../../../components/shared/AdminWrapper/AdminWrapper";
import { AppContext } from "../../../App";
import { executeHardUpdate, executeLightUpdate, recreateSystemTables, recreateUserTable } from "../../../api/db.api";

const LOADER_ID = "DB_LOADER";

export default function AdminConfiguration() {

    const { addLoader, removeLoader, addErrorMessage } = useContext(AppContext);
    
    const callHardUpdate = async () => {
        addLoader(LOADER_ID);
        
        try {
            await executeHardUpdate();
        } catch (e) {
            addErrorMessage(e.data.message);
        } finally {
            removeLoader(LOADER_ID);
        }
    };

    const callLightUpdate = async () => {
        addLoader(LOADER_ID);

        try {
            await executeLightUpdate();
        } catch (e) {
            addErrorMessage(e.data.message);
        } finally {
            removeLoader(LOADER_ID);
        }
    };

    const callSystemTablesRecreation = async () => {
        addLoader(LOADER_ID);

        try {
            await recreateSystemTables();
        } catch (e) {
            addErrorMessage(e.data.message);
        } finally {
            removeLoader(LOADER_ID);
        }
    };

    const callUsersTableRecreation = async () => {
        addLoader(LOADER_ID);

        try {
            await recreateUserTable();
        } catch (e) {
            addErrorMessage(e.data.message);
        } finally {
            removeLoader(LOADER_ID);
        }
    };
    
    return(
        <AdminWrapper>
            <div className="admin-configuration">
                <button type="button" className="admin-configuration__action-button" onClick={callHardUpdate}>Обновить полностью</button>
                <button type="button" className="admin-configuration__action-button" onClick={callLightUpdate}>Обновить на 2 недели</button>
                <button type="button" className="admin-configuration__action-button" onClick={callSystemTablesRecreation}>Пересоздать системные таблицы</button>
                <button type="button" className="admin-configuration__action-button" onClick={callUsersTableRecreation}>Пересоздать таблицу с пользователями</button>
            </div>
        </AdminWrapper>
    );
}
