import React from 'react';
import Cookies from "cookies-ts";
import './style.scss';
import { NavLink } from 'react-router-dom';
import AdminWrapper from "../../../components/shared/AdminWrapper/AdminWrapper";
import { PATHS } from "../../../utils/constants";

const cookies = new Cookies();

export default function AdminMain() {

    const role = cookies.get("role");

    return(
        <AdminWrapper>
            <div className="admin-main__sections">
                <NavLink to={PATHS.adminEntities} className="admin-main__section-link">
                    <div className="admin-main__section-card">
                        <div className="icon entities"/>
                        <p>Управление сущностями</p>
                    </div>
                </NavLink>
                {
                    role === "SUPER_ADMIN" &&
                    <NavLink to={PATHS.adminConfiguration} className="admin-main__section-link">
                        <div className="admin-main__section-card">
                            <div className="icon settings"/>
                            <p>Конфигурация БД</p>
                        </div>
                    </NavLink>
                }
                {
                    role === "SUPER_ADMIN" &&
                    <NavLink to={PATHS.adminUsers} className="admin-main__section-link">
                        <div className="admin-main__section-card">
                            <div className="icon users"/>
                            <p>Управление пользователями</p>
                        </div>
                    </NavLink>
                }
            </div>
        </AdminWrapper>
    );
}
