import React from 'react';
import './style.scss';
import AdminLoginForm from "../../../components/control/AdminLoginForm/AdminLoginForm";
import AdminLabel from "../../../components/shared/AdminLabel/AdminLabel";

export default function AdminLogin() {
    return(
        <div className="admin-login__wrapper">
            <AdminLabel/>
            <AdminLoginForm/>
        </div>
    );
}
