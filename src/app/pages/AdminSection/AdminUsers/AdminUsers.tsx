import React, { useCallback, useContext, useEffect, useState } from 'react';
import './style.scss';
import AdminWrapper from "../../../components/shared/AdminWrapper/AdminWrapper";
import { deleteUserById, getAllUsers } from "../../../api/users.api";
import { AppContext } from "../../../App";
import { UserResponse } from "../../../models/response/UserResponse";
import * as classNames from "classnames";
import CreateUserForm from "../../../components/control/CreateUserForm/CreateUserForm";

const USERS_LOADER_ID = "USERS_LOADER";

export default function AdminUsers() {

    const { addLoader, removeLoader, addErrorMessage } = useContext(AppContext);
    const addLoaderCallback = useCallback(addLoader, []);
    const removeLoaderCallback = useCallback(removeLoader, []);
    const addErrorMessageCallback = useCallback(addErrorMessage, []);
    
    const [users, setUsers] = useState<UserResponse[]>([]);
    const [showUserForm, setShowUserForm] = useState(false);

    useEffect(() => {
        addLoaderCallback(USERS_LOADER_ID);

        getAllUsers()
            .then(res => {
                setUsers(res.data);
            })
            .catch(err => {
                addErrorMessageCallback(err);
            })
            .finally(() => {
                removeLoaderCallback(USERS_LOADER_ID);
            });

    }, [addErrorMessageCallback, addLoaderCallback, removeLoaderCallback]);

    const onUserDeleteClick = async (id: number | undefined) => {
        if (!id) return;

        addLoader(USERS_LOADER_ID);

        try {
            await deleteUserById(id);
            const response = await getAllUsers();
            setUsers(response.data);
        } catch (e) {
            addErrorMessage(e);
        } finally {
            removeLoader(USERS_LOADER_ID);
        }
    };
    
    return(
        <>
            <AdminWrapper>
                <div className="admin-users">
                    <div className="admin-users__item">
                        <p className="admin-users__item-text _title">Имя</p>
                        <p className="admin-users__item-text _title">Логин</p>
                        <p className="admin-users__item-text _title">Роль</p>
                        <button type="button"
                                className="admin-users__item-remove-button _title"/>
                    </div>
                    {
                        users.map(user => (
                            <div className="admin-users__item" key={user.id}>
                                <p className="admin-users__item-text">{user.name}</p>
                                <p className="admin-users__item-text">{user.login}</p>
                                <p className="admin-users__item-text">{user.role}</p>
                                <button type="button"
                                        className={classNames(
                                            "admin-users__item-remove-button",
                                            { admin: user.login === "admin" }
                                        )}
                                        onClick={() => onUserDeleteClick(user.id)}>

                                </button>
                            </div>
                        ))
                    }
                </div>
                <button type="button"
                        className="admin-users__create"
                        onClick={() => setShowUserForm(true)}>Добавить пользователя</button>
            </AdminWrapper>
            { showUserForm && <CreateUserForm onClose={() => setShowUserForm(false)}/> }
        </>
    );
}
