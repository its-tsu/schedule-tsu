import React, { createContext, useState } from 'react';
import './style.scss';
import { Routes } from './routes';
import Header from './components/views/Header/Header';
import Footer from './components/views/Footer/Footer';
import RootLoader from "./components/shared/RootLoader/RootLoader";
import { useLocation } from "react-router-dom";
import { PATHS } from "./utils/constants";
import NotificationCard from "./components/shared/NotificationCard/NotificationCard";

const appContext: {
    addLoader: ((loaderId: string) => void);
    removeLoader: ((loaderId: string) => void);
    addErrorMessage: ((message: string) => void);
} = {
    addLoader: () => console.error("loader is not initialised"),
    removeLoader: () => console.error("loader is not initialised"),
    addErrorMessage: () => console.error("error pane is not initialised")
};

export const AppContext = createContext(appContext);

export default function App() {

    let timeout = 0;

    const location = useLocation();
    const [loadersList, setLoaderState] = useState<Array<string>>([]);
    const [errorMessage, setErrorMessage] = useState("");

    appContext.addLoader = (loaderId: string) => setLoaderState(prevState => [...prevState, loaderId]);
    appContext.removeLoader = (loaderId: string) => {
        setLoaderState(prevState => {
            const index = prevState.indexOf(loaderId);
            if (index >= 0) {
                prevState.splice(index, 1);
                return [...prevState];
            }
            return prevState;
        });
        setTimeout(() => window.scrollTo(0, 0), 100);
    };

    appContext.addErrorMessage = (errorMessage: string) => {
        if (!timeout) {
            window.clearTimeout(timeout);
        }

        setErrorMessage(errorMessage);

        timeout = window.setTimeout(() => setErrorMessage(""), 3000);
    };

    const onErrorClose = () => {
        if (!timeout) {
            window.clearTimeout(timeout);
        }

        setErrorMessage("");
    };

    return(
        <div className="site-wrapper">
            <AppContext.Provider value={appContext}>
                <Header/>
                <NotificationCard message={errorMessage} onClose={onErrorClose}/>
                <Routes/>
                {
                    location.pathname.includes(PATHS.terminalsRoot) || <Footer/>
                }
                {
                    loadersList.length > 0 && <RootLoader/>
                }
            </AppContext.Provider>
        </div>
    );
}
