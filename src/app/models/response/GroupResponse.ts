import { InstituteResponse } from "./InstituteResponse";

export interface GroupResponse {
    id: number;
    name: string;
    course: string;
    institute: InstituteResponse
}