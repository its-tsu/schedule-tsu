export class SearchResponse {
    id: number;
    type: string;
    name: string;

    constructor(id: number, name: string, type: string) {
        this.id = id;
        this.name = name;
        this.type = type;
    }
}