import { TeachersResponse } from "./TeachersResponse";
import { ClassroomResponse } from "./ClassroomResponse";

export interface TeacherClassroomsResponse {
    teacher: TeachersResponse,
    classroom: ClassroomResponse
}