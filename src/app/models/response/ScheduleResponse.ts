import { GroupResponse } from "./GroupResponse";
import { TeachersResponse } from "./TeachersResponse";
import { ClassroomResponse } from "./ClassroomResponse";

export interface ScheduleResponse {
    id: string;
    disciplineName: string;
    type: string;
    pairNumber: number;
    date: string;
    fromTime: string;
    toTime: string;
    note?: string;
    link: string;
    teacher?: TeachersResponse,
    classroom?: ClassroomResponse
    groupsList: Array<GroupResponse>;
}