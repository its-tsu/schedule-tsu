export interface UserResponse {
    id?: number;
    name: string;
    login: string;
    password: string;
    role: string;
}