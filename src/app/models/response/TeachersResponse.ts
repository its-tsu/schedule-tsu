import { DepartmentResponse } from "./DepartmentResponse";

export interface TeachersResponse {
    id: number;
    name: string;
    lastName: string;
    patronymic: string;
    department: DepartmentResponse
}