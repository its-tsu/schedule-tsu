import { BaseModel } from "../BaseModel";

export class DepartmentResponse extends BaseModel {
    institute: BaseModel;

    constructor(id: number, name: string, institute: BaseModel) {
        super(id, name);
        this.institute = institute;
    }
}