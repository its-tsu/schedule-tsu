import { BuildingsResponse } from "./BuildingsResponse";

export interface ClassroomResponse {
    id: number;
    name: string;
    number: string;
    floor: string;
    building: BuildingsResponse;
}