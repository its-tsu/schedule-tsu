export enum ScheduleTypes {
    GROUP = "group",
    TEACHER = "teacher",
    CLASSROOM = "classroom",
    TEST_CENTER = "test-center"
}