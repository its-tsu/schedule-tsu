import { BaseModel } from "./BaseModel";

export class SearchParamBaseModel extends BaseModel {
    courseAltId?: string;

    constructor(id = 0, name: string) {
        super(id, name);

        this.courseAltId = id === 0 ? name : undefined;
    }
}