import { TeachersResponse } from "../models/response/TeachersResponse";
import moment from "moment";
import {ScheduleTypes} from "../models/enums/ScheduleTypes";
import {ClassroomResponse} from "../models/response/ClassroomResponse";

export const ARRAYS = {
    sortByString: function (a: string, b: string) {
        return ('' + a).localeCompare(b);
    }
};

export const DATES = {
    daysOfWeek: ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'],
    getPairTimeByDates: function (fromDate: string, toDate: string): string {
        const fromTime = moment.utc(fromDate).format("HH:mm");
        const toTime = moment.utc(toDate).format("HH:mm");

        return fromTime + "-" + toTime;
    }
};

export const STRINGS = {
    getTeacherShortName: (teacher: TeachersResponse) => `${teacher.lastName} ${teacher.name[0]}.${teacher.patronymic[0]}.`,
    getClassroomName: (classroom: ClassroomResponse, note: string | null | undefined, type: string) => {
        if (type === ScheduleTypes.GROUP) {
            if (note) {
                return note;
            }

            return classroom.name;
        }

        return classroom.number;
    }
};

export const BROWSER = {
    isMobile: () => /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
};

export const INPUT_LABEL_ID = {
    id: 0,
    newId: function (prefix = 'id') {
        this.id++;
        return `${prefix}${this.id}`;
    }
};

export const URL = {
    getJsonFromUrl: function (search: string): string {

        const query = search.substr(1);
        const result = {};

        query.split("&").forEach(function (part) {
            const item = part.split("=");
            let decodedItem: string | boolean = decodeURIComponent(item[1]);

            if (decodedItem === "true" || decodedItem === "false") {
                decodedItem = decodedItem === "true";
            }

            result[item[0]] = decodedItem;
        });

        return JSON.stringify(result);
    }
};