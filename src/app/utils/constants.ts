import { ScheduleTypes } from "../models/enums/ScheduleTypes";

export const PATHS = {
    root: '/',
    adminRoot: "/admin",
    adminEntities: "/admin/entities",
    adminConfiguration: "/admin/configuration",
    adminUsers: "/admin/users",
    terminalsRoot: "/terminal",
    studentsInstitutesList: '/' + ScheduleTypes.GROUP + '-institutes',
    teachersInstitutesList: '/' + ScheduleTypes.TEACHER + '-institutes',
    classroomsInstitutesList: '/' + ScheduleTypes.CLASSROOM + '-institutes',
    testsCenterInstitutesList: '/' + ScheduleTypes.TEST_CENTER + '-institutes',
    studentsGroupsList: '/' + ScheduleTypes.GROUP + '-groups/:instId/:instName',
    teachersGroupsList: '/' + ScheduleTypes.TEACHER + '-groups/:instId/:instName',
    classroomsGroupsList: '/' + ScheduleTypes.CLASSROOM + '-groups/:instId/:instName',
    groupSchedule: '/' + ScheduleTypes.GROUP + '/:id',
    teacherSchedule: '/' + ScheduleTypes.TEACHER + '/:id',
    classroomSchedule: '/' + ScheduleTypes.CLASSROOM + '/:id',
    adminLogin: "/login",
    error404: "/error-404"
};

export const LINKS = {
    tsu: 'https://tltsu.ru',
    its: 'https://it-student.ru/',
    support: 'https://vk.com/id105171916'
};

export const BREAKPOINTS = {
    laptopsAdaptive: 1200,
    portraitTabletsAdaptive: 990,
    landscapePhonesAdaptive: 768,
    portraitPhonesAdaptive: 480,
};