import { GroupResponse } from "../../models/response/GroupResponse";
import { TeachersResponse } from "../../models/response/TeachersResponse";
import { ClassroomResponse } from "../../models/response/ClassroomResponse";
import { BaseModel } from "../../models/BaseModel";

export class BaseModelConverter {
    getRootItem(item: GroupResponse | TeachersResponse | ClassroomResponse): BaseModel {
        if ("institute" in item) {
            return item.institute;
        }

        if ("department" in item) {
            return item.department.institute;
        }

        if ("building" in item) {
            return item.building;
        }

        return new BaseModel(0, "");
    }

    getItemsGroup(item: GroupResponse | TeachersResponse | ClassroomResponse): BaseModel {
        if ("course" in item) {
            return new BaseModel(0, item.course);
        }

        if ("department" in item) {
            return item.department;
        }

        if ("building" in item) {
            return new BaseModel(0, item.floor);
        }

        return new BaseModel(0, "");
    }
}