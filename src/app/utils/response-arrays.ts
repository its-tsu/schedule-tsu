import { FormattedItem } from '../classes/search.classes';
import { Teacher } from '../classes/teachers.classes';

export function formatTestCenterResponseArray(testCenterList: Array<Teacher>) {
    return testCenterList.map(testCenter => (
        new FormattedItem(testCenter.prepodId, testCenter.prepodName, "test-centers")
    ));
}