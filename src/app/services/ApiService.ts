import moment, { Moment } from "moment";
import { ScheduleResponse } from "../models/response/ScheduleResponse";
import { ScheduleTypes } from "../models/enums/ScheduleTypes";
import { getScheduleForClassroom, getScheduleForGroup, getScheduleForTeacher } from "../api/schedule.api";
import { BaseModel } from "../models/BaseModel";
import {
    getBuildingsList,
    getClassroomById,
    getClassroomsByBuildingAndFloor,
    getFloorsByBuildingId
} from "../api/classrooms.api";
import { getInstitutesList } from "../api/institutes.api";
import { TeachersResponse } from "../models/response/TeachersResponse";
import { getTeacherById, getTeacherDepartmentsByInstId, getTeachersByDepartment } from "../api/teachers.api";
import { getCoursesList } from "../api/courses.api";
import { GroupResponse } from "../models/response/GroupResponse";
import { ClassroomResponse } from "../models/response/ClassroomResponse";
import { getGroupById, getGroupsListByInstIdAndCourseName } from "../api/groups.api";
import { DepartmentResponse } from "../models/response/DepartmentResponse";
import { CancelTokenSource } from "axios";

export class ApiService {

    async getListItems(type: string, rootItemId: number, searchParam: number | string): Promise<BaseModel[]> {
        switch (type) {
            case ScheduleTypes.TEACHER: {
                let teachersList: Array<TeachersResponse>;

                try {
                    teachersList = await getTeachersByDepartment(Number(searchParam));
                } catch (e) {
                    throw e;
                }

                return teachersList
                    .map(teacher => new BaseModel(teacher.id, `${teacher.lastName} ${teacher.name} ${teacher.patronymic}`));
            }
            case ScheduleTypes.CLASSROOM: {
                let classRoomsList: Array<ClassroomResponse>;

                try {
                    classRoomsList = await getClassroomsByBuildingAndFloor(rootItemId, searchParam.toString());
                } catch (e) {
                    throw e;
                }

                return classRoomsList.map(classroom => new BaseModel(classroom.id, classroom.number));
            }
            case ScheduleTypes.GROUP: {
                return getGroupsListByInstIdAndCourseName(rootItemId, searchParam.toString());
            }
            default: return [];
        }
    }

    async getItemByIdAndType(id: number, type: string): Promise<GroupResponse | TeachersResponse | ClassroomResponse | null> {
        switch (type) {
            case ScheduleTypes.CLASSROOM: {
                return getClassroomById(id);
            }
            case ScheduleTypes.TEACHER: {
                return getTeacherById(id);
            }
            case ScheduleTypes.GROUP: {
                return getGroupById(id);
            }
            default: return null;
        }
    }

    async getRootItemsList(type: string): Promise<Array<BaseModel>> {
        switch (type) {
            case ScheduleTypes.CLASSROOM: {
                return getBuildingsList();
            }
            default: {
                return getInstitutesList();
            }
        }
    }

    async getItemsList(type: string, rootItemId: number): Promise<Array<BaseModel>> {
        switch (type) {
            case ScheduleTypes.CLASSROOM: {
                try {
                    const floorsList: Array<string> = await getFloorsByBuildingId(rootItemId);
                    return floorsList.map(floor => new BaseModel(0, floor));
                } catch (e) {
                    throw e;
                }
            }
            case ScheduleTypes.TEACHER: {
                try {
                    const departmentsList: Array<DepartmentResponse> = await getTeacherDepartmentsByInstId(rootItemId);
                    return departmentsList.map(department => new BaseModel(department.id, department.name));
                } catch (e) {
                    throw e;
                }
            }
            case ScheduleTypes.GROUP: {
                try {
                    const coursesList: Array<string> = await getCoursesList(rootItemId);
                    return coursesList.map(course => new BaseModel(0, course));
                } catch (e) {
                    throw e;
                }
            }
            default: return [];
        }
    }

    async getScheduleByTypeAndId(type: string, id: number, date: Moment, isWeek: boolean, source: CancelTokenSource): Promise<Array<ScheduleResponse>> {
        const dates = ApiService.getRequestDates(date, isWeek);

        switch (type) {
            case ScheduleTypes.CLASSROOM: {
                return getScheduleForClassroom(id, dates.fromDate, dates.toDate, source);
            }
            case ScheduleTypes.GROUP: {
                return getScheduleForGroup(id, dates.fromDate, dates.toDate, source);
            }
            case ScheduleTypes.TEACHER: {
                return getScheduleForTeacher(id, dates.fromDate, dates.toDate, source);
            }
            default: return [];
        }
    }

    private static getRequestDates(date: Moment, isWeek: boolean): { fromDate: string, toDate: string } {

        if (isWeek) {
            return {
                fromDate: date.clone().startOf("isoWeek").toISOString(),
                toDate: date.clone().endOf("isoWeek").toISOString()
            };
        }

        return {
            fromDate: moment().year(date.year()).month(date.month()).date(date.date()).toISOString(),
            toDate: moment().year(date.year()).month(date.month()).date(date.date()).toISOString()
        };
    }
}